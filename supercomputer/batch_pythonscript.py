#! /usr/bin/env python3

# todo : refactor everything in a same folder (simulation, stats, post process)


def ReadFixedEffect(file):
    """ Read the mean estimates from a file containing the statistical analysis of the peak to peak data.
        input : 
            - file : a string providing the file where the information must be read 
                    (one file corresponds to a type of mouse, type of vessel, type of measure, frequency band selected)

        output :  a dict. The keys corresponds to the stages. The values are float of the estimates.
                For the stage 'baseline', the value is the Intercept value.
                After discussion with Rune, this is not used anymore : If the stage is not baseline, then the value is a different value from baseline only if the p value for this stage is < 0.05
                meaning that this stage is significantly different from the baseline.
                
    """
    import numpy as np

    d={}
    with open(file) as f:
        line=f.readline()# skip first line
        # read data
        line=f.readline()
        lines=[]
        while line:
            line=line.split('\n')[0]
            lines.append(line.split(' '))
            line = f.readline()
    
    #get the intecept value (correspond to the log of the variable)
    intercept=float(lines[0][1])

    #the baseline value correspond to the exp of the intercept
    d['baseline']=np.exp(intercept)

    # the other stages we add the estimate to the intecept.   NOT USED ANYMORE : only if significative difference with baseline
    for line in lines[1::] :
        d[line[0].replace('"', '')]=np.exp(intercept+float(line[1]))#*(float(line[3])<0.05))

    return d


def ReadRandomEffect(file):
    """ Read the mean estimates from a file containing the statistical analysis of the peak to peak data.
        input : 
            - file : a string providing the file where the information must be read 
                    (one file corresponds to a type of mouse, type of vessel, type of measure, frequency band selected)

        output :  a dict. The keys corresponds to the stages. 
        The values are np arrays of the estimates (floats) for each vessel.

    """
    import numpy as np

    d={}
    with open(file) as f:
        # first line contains the name of the stages
        stages=f.readline()
        stages=stages.split('\n')[0]
        stages=stages.split(' ')
        # read data
        line=f.readline()
        lines=[]
        while line:
            line=line.split('\n')[0] 
            lines.append(line.split(' '))
            line = f.readline()
    

    # intercepts for each vessel
    d['baseline']={}
    for line in lines:
        vesselid=line[0]
        vesselid=vesselid.replace('"', '')
        vesselid=vesselid.replace('.', '-')
        #get the intecept value (correspond to the log of the variable)
        intercept=float(line[1])
        d['baseline'][vesselid]=np.exp(intercept)
        #the baseline value correspond to the exp of the intercepts
    
    for columnno,stage in enumerate(stages[1::]) :
        stage=stage.replace('"', '')
        d[stage]={}
        #pvalues=[]
        for line in lines:
            #get the estimates value (correspond to the log of the variable)
            intercept=float(line[1])
            estimate=line[columnno+2]# first two columns are for vessel id and intercept
            vesselid=line[0]
            vesselid=vesselid.replace('"', '')
            vesselid=vesselid.replace('.', '-')
            try :
                estimate=float(estimate)
            except :
                estimate=0.
                print('aie aie aie')
                
            ### WARNING : The random effect files do not need estimate to be added to intercept !!!
            d[stage][vesselid]=np.exp(estimate)

    

    return d


def get_slurmtemplate (jobname,templatefile='template.slurm'):
    
    
    with open(templatefile, 'r') as f:

        slurm_template=[]
        slurm_template+='#!/bin/bash\n'
        slurm_template+='# Job name:\n'
        slurm_template+='#SBATCH --job-name='+jobname+'\n'
        slurm_template+='#\n'
        slurm_template+='# Project:\n'
        slurm_template+='#SBATCH --account=NN9279K\n'
        slurm_template+='#\n'
        slurm_template+='# Wall clock limit:\n'
        slurm_template+='#SBATCH --time=50:00:00\n'
        slurm_template+='#\n'
        slurm_template+='#SBATCH --partition=bigmem\n'
        slurm_template+='#\n'
        slurm_template+='# Memory per CPU:\n'
        slurm_template+='#SBATCH --mem-per-cpu=10G\n'
        slurm_template+='#\n'
        slurm_template+='# Number of processes:\n'
        slurm_template+='#SBATCH --ntasks=1\n'
        slurm_template+='#\n'

        slurm_template.extend(f.readlines())

        
        return slurm_template



    
def base_commandline(lpvs=200e-4, c0init='constant', c0valuePVS=50, c0valueSAS=0, sigma=1e-4, sasbc='scenarioB', tend=800, toutput=1, dt=1e-3, r=-1, nr=8, nl=100, d=2e-7, refineleft=True, sas=False) :
    """
    Create a base for the command line

    Parameters
    ----------
    lpvs : FLOAT, optional
        length of the vessel. The default is 200e-4.
    c0init : STRING,optional
        kind of initialisation for the concentration . 
        The default is 'null' which means the initial concentration is zero everywhere in the PVS. 
        If set to 'constant' the concentration value in the PVS will be c0value.
    c0value : FLOAT, optional
        Initial value for the concentration in the SAS and in the PVS depending on c0init. The default is 50.
    sasbc : STRING, optional
        Kind of boundary condition in the SAS. 
        The default is 'scenarioB' : mass conservation, no outflow of CSF out of the SAS.
        'scenarioA' : zero concentration in the SAS - the tracer is flushed out.
        'scenarioB': massconservation no outflow
        'scenarioC' : constant CSF outflow.
        'scenarioD' : pressure dependent outflow.
    tend : FLOAT, optional
        end time of the simulation. The default is 800.
    toutput : FLOAT, optional
        period for the outputs. The default is 1.
    dt : FLOAT, optional
        time step. The default is 1e-3.
    r : FLOAT, optional
        Flow resistance at the right boundary. 
        The default is -1 which leads to a zero flow condition.
    nr : INT, optional
        Number of cells in the radial direction. The default is 8.
    nl : INT, optional
        number of cells in the longitudinal direction. The default is 100.
    d : FLOAT, optional
        Modelecular diffusion coefficient. The default is 2e-7.
    refineleft : BOOL, optional
        State if the mesh must have a geometrical progression of the left side. 
        The default is True.

    Returns
    -------
    STRING : base for the command line

    """
    

    
    jobcommand='srun -n 1 python3 PVS_cyclessimulation.py'

    jobcommand+=' -lpvs '+str(lpvs)
    jobcommand+=' -c0init '+c0init
    jobcommand+=' -c0valueSAS '+str(c0valueSAS)
    jobcommand+=' -c0valuePVS '+str(c0valuePVS)
    jobcommand+=' -sasbc '+sasbc
    jobcommand+=' -tend '+str(tend)
    jobcommand+=' -toutput '+str(toutput)
    jobcommand+=' -dt '+str(dt)
    jobcommand+=' -r '+str(r)
    jobcommand+=' -nr '+str(nr)
    jobcommand+=' -nl '+str(nl)
    jobcommand+=' -d '+str(d)
    if refineleft:
        jobcommand+=' -refineleft '+str(refineleft)
    jobcommand+=' -s '+str(sigma)
    if sas :
        jobcommand+=' -issas '+str(sas)
    
    if c0init=='gaussian':
         jobcommand+=' -xi '+str(lpvs/2)
    
    return jobcommand


def base_PVSBraincommandline(lpvs=200e-4, mesh='regular', Emem=10e4, E=10e4, K=1e-13, Kmem=1e-15, hmem=1e-4, tend=20, toutput=1e-1, toutputcycle=1e-2, dt=5e-3, r=-1, nrpvs=6, nrmem=4,nl=100, d=2e-7,rbrain=100e-4) :
    """
    Create a base for the command line

    Parameters
    ----------
    lpvs : FLOAT, optional
        length of the vessel. The default is 200e-4.
    c0init : STRING,optional
        kind of initialisation for the concentration . 
        The default is 'null' which means the initial concentration is zero everywhere in the PVS. 
        If set to 'constant' the concentration value in the PVS will be c0value.
    c0value : FLOAT, optional
        Initial value for the concentration in the SAS and in the PVS depending on c0init. The default is 50.
    sasbc : STRING, optional
        Kind of boundary condition in the SAS. 
        The default is 'scenarioB' : mass conservation, no outflow of CSF out of the SAS.
        'scenarioA' : zero concentration in the SAS - the tracer is flushed out.
        'scenarioB': massconservation no outflow
        'scenarioC' : constant CSF outflow.
        'scenarioD' : pressure dependent outflow.
    tend : FLOAT, optional
        end time of the simulation. The default is 800.
    toutput : FLOAT, optional
        period for the outputs. The default is 1.
    dt : FLOAT, optional
        time step. The default is 1e-3.
    r : FLOAT, optional
        Flow resistance at the right boundary. 
        The default is -1 which leads to a zero flow condition.
    nr : INT, optional
        Number of cells in the radial direction. The default is 8.
    nl : INT, optional
        number of cells in the longitudinal direction. The default is 100.
    d : FLOAT, optional
        Modelecular diffusion coefficient. The default is 2e-7.
    refineleft : BOOL, optional
        State if the mesh must have a geometrical progression of the left side. 
        The default is True.

    Returns
    -------
    STRING : base for the command line

    """
    

    
    jobcommand='srun -n 1 python3 PVSBrainMembrane_nitsche.py'

    jobcommand+=' -lpvs '+str(lpvs)
    jobcommand+=' -tend '+str(tend)
    jobcommand+=' -toutput '+str(toutput)
    jobcommand+=' -toutputcycle '+str(toutputcycle)
    jobcommand+=' -dt '+str(dt)
    jobcommand+=' -rbrain '+str(rbrain)
    jobcommand+=' -nrpvs '+str(nrpvs)
    jobcommand+=' -nrmem '+str(nrmem)
    jobcommand+=' -mesh '+str(mesh)
    jobcommand+=' -nl '+str(nl)
    jobcommand+=' -d '+str(d)
    jobcommand+=' -perm '+str(K)
    jobcommand+=' -permmem '+str(Kmem)
    jobcommand+=' -E '+str(E)
    jobcommand+=' -Emem '+str(Emem)
    jobcommand+=' -hmem '+str(hmem)
    

    return jobcommand



def write_cycle_slurm(jobname, cycle, outputfolder, file, **kargs) :
    """
    Function to write a slurm file with the proper command line to launch a simulation of successive oscillation cycles.


    Parameters
    ----------
    jobname : STRING
        name of the job : will be used as label for the outputfiles.
    cycle : STRING
        name of the oscillations cycles to simulate. Must be defined in the cycles.yml file.
    file : STRING
        name of the slurm file to create.


    Returns
    -------
    None. The function creates a file.

    """
    
    
    jobcommand=base_commandline(**kargs)
    jobcommand+=' -j '+jobname
    jobcommand+=' -cycle '+cycle
    
    jobcommand+=' -o '+outputfolder
    
    slurm_template=get_slurmtemplate(jobname)
    
    with open(file, 'w') as f:
        for line in slurm_template :
            f.write(line)
        f.write('\n')
        f.write(jobcommand)
        
    f.close()
    
def write_state_slurm(jobname, fi, ai, rv, h0, outputfolder, file, **kargs) :
    """
    Function to write a slurm file with the proper command line to launch a simulation of a given oscillatory state.


    Parameters
    ----------
    jobname : STRING
        name of the job : will be used as label for the outputfiles.
    fi: LIST of FLOAT
        provides the frequencies (Hz) of the oscillations to be superposed
    ai: LIST of FLOAT
        provides the amplitudes (ratio) of the oscillations to be superposed
    Rv: FLOAT
        vessel mean radius (cm)
    h0: FLOAT
        PVS mean thickness (cm)
    file : STRING
        name of the slurm file to create.
    

    Returns
    -------
    None. The function creates a file.

    """
    
    
    jobcommand=base_commandline(**kargs)
    jobcommand+=' -j '+jobname
    jobcommand+=' -ai '
    
    for a in ai :
        jobcommand+=str(a)+' ' 
        
    jobcommand+=' -fi '
    for f in fi :
        jobcommand+=str(f)+' ' 
        
    jobcommand+=' -rv '+str(rv)
    jobcommand+=' -rpvs '+str(rv+h0)
    
    jobcommand+=' -o '+outputfolder
    
    slurm_template=get_slurmtemplate(jobname)
    
    with open(file, 'w') as f:
        for line in slurm_template :
            f.write(line)
        f.write('\n')
        f.write(jobcommand)
        
    f.close()
    

def write_PVSBrain_slurm(jobname, fi, ai, rv, h0, outputfolder, file, **kargs) :
    """
    Function to write a slurm file with the proper command line to launch a simulation of a given oscillatory state.


    Parameters
    ----------
    jobname : STRING
        name of the job : will be used as label for the outputfiles.
    fi: LIST of FLOAT
        provides the frequencies (Hz) of the oscillations to be superposed
    ai: LIST of FLOAT
        provides the amplitudes (ratio) of the oscillations to be superposed
    Rv: FLOAT
        vessel mean radius (cm)
    h0: FLOAT
        PVS mean thickness (cm)
    file : STRING
        name of the slurm file to create.
    

    Returns
    -------
    None. The function creates a file.

    """
    
    
    jobcommand=base_PVSBraincommandline(**kargs)
    jobcommand+=' -j '+jobname
    jobcommand+=' -ai '
    
    for a in ai :
        jobcommand+=str(a)+' ' 
        
    jobcommand+=' -fi '
    for f in fi :
        jobcommand+=str(f)+' ' 
        
    jobcommand+=' -rv '+str(rv)
    jobcommand+=' -rpvs '+str(rv+h0)
    
    jobcommand+=' -o '+outputfolder
    
    slurm_template=get_slurmtemplate(jobname,templatefile='templategmsh.slurm')
    
    with open(file, 'w') as f:
        for line in slurm_template :
            f.write(line)
        f.write('\n')
        f.write(jobcommand)
        
    f.close()

    
import numpy as np
def stokeeinstein(radius,kb=1.3806488e-23 , Tcelsius= 35, viscosity=0.693e-3 ):

    
    radius=np.array(radius)*1e-9 # nm to m
    
    Tkelvin=273.25+Tcelsius
    
    D=kb*Tkelvin/(6*np.pi*viscosity*radius)*1e4 # in cm2/s
    
    return list(D)
    
    
    
if __name__ == '__main__':
    
    
    import os
    
    
    # Physical properties
    # The molecular dimensions of some Dextran fractions
    dextranradius={10:2.36,40:4.45,50:4.95,70:5.8,100:6.9,200:9.5,500:14.7,1000:19.9,2000:27, 0:5.8*10} # in nm
    # ast one correspond to the 70 kDA * 10 to take into account obstacles.
    
    
    
    ### Penetrating arterioles analysis

    # Cycle analysis
    #---------------------------------------------
    
    # We would like to cover those parameters range
    spanlpvs=[200e-4,400e-4]
    spandextran=[10,70,2000, 0]
    
    spanlpvs=[200e-4, 400e-4, 600e-4]
    spandextran=[0,70]    
    spansasbc=['scenarioA']  
    spandiffusion=stokeeinstein([dextranradius[k] for k in spandextran])
    
    
    spandiffusion=[0.84e-7*2,0.068e-6]
    
    
    spancycles=['normalsleep','quietwake']
    
    
    # serie name
    serie='cyclePART1'
    
    # create a folder for the slurm files and the batch file
    if not os.path.exists(serie):
        os.makedirs(serie)
    seriedir=serie+'/'
    
    slurmfiles=[]
    for cycle in spancycles :
        for lpvs in spanlpvs:
            for d in spandiffusion :
                for sasbc in spansasbc : 
                    jobname=cycle+'-d%.0e'%d+'-l%.0e'%lpvs+'-'+sasbc
    
                    slurmfile=jobname+'.slurm'
                    write_cycle_slurm(jobname, cycle, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, sasbc=sasbc, lpvs=lpvs,d=d,dt=5e-3,tend=400)
                                   
                    #update the list of slurm files to be launched in batch
                    slurmfiles.append(slurmfile)

    
    #write the batch file
    with open(seriedir+'batch','w') as f :
        for slurmfile in slurmfiles :
            f.write('sbatch '+slurmfile+' &\n')
    f.close()
    
    
    # serie name
    serie='cyclePART2'
    
    spancycles=['noarrousalsleep','REMsleep','NREMsleep']
    
    # create a folder for the slurm files and the batch file
    if not os.path.exists(serie):
        os.makedirs(serie)
    seriedir=serie+'/'
    
    slurmfiles=[]
    for cycle in spancycles :
        for lpvs in spanlpvs:
            for d in spandiffusion :
                for sasbc in spansasbc : 
                    jobname=cycle+'-d%.0e'%d+'-l%.0e'%lpvs+'-'+sasbc
    
                    slurmfile=jobname+'.slurm'
                    write_cycle_slurm(jobname, cycle, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, sasbc=sasbc, lpvs=lpvs,d=d,dt=5e-3,tend=400)
                                   
                    #update the list of slurm files to be launched in batch
                    slurmfiles.append(slurmfile)

    
    #write the batch file
    with open(seriedir+'batch','w') as f :
        for slurmfile in slurmfiles :
            f.write('sbatch '+slurmfile+' &\n')
    f.close()
        
    
    #numerical convergence test
    
    spandt=[10e-3,5e-3,2.5e-3, 1e-3]
    
    spannrnl=[(16,200),(8,100),(4,50)]
    
    # serie name
    serie='cycleConvergence'
    
    # create a folder for the slurm files and the batch file
    if not os.path.exists(serie):
        os.makedirs(serie)
    seriedir=serie+'/'
    
    # we perform the convergence test of normal sleep
    cycle='normalsleep'
    lpvs=200e-4
    d=spandiffusion[0]
    
    slurmfiles=[]

    for dt in spandt:
        for (nr,nl) in spannrnl :
            jobname=cycle+'-dt%.0e'%dt+'-nr%i'%nr+'-nl%i'%nl

            slurmfile=jobname+'.slurm'
            write_cycle_slurm(jobname, cycle, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt,nr=nr,nl=nl,tend=400, sasbc='scenarioB', c0init='constant',c0valuePVS=50, c0valueSAS=0)
                               
            #update the list of slurm files to be launched in batch
            slurmfiles.append(slurmfile)

    
    #write the betch file
    with open(seriedir+'batch','w') as f :
        for slurmfile in slurmfiles :
            f.write('sbatch '+slurmfile+' &\n')
    f.close()
    
    
    # PVS deformation analysis
    #-------------------------------------
    # For the mean values of lumen cardiac pulsation, we run the PVS + Tissue simulation to assess PVS deformation and max CSF velocity
    #
    # serie name
    serie='deformationWT9'
    
    # create a folder for the slurm files and the batch file
    if not os.path.exists(serie):
        os.makedirs(serie)
    seriedir=serie+'/'    
        
    # for each vessels estimates.
     
    # sleep data
    folder='/home/alexandra/Documents/Data/Celine/penetrating_arterioles_WT9_onlyWT_includingArea/'
    vessel='PenetratingArtrioles'
    mouse='WT9_'
    analysis='sleep_'
    ftype='fixedEffects.txt'
      
    sleepdata={}
    sleepdata['Rv']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'medianepisode_'+ftype)
    sleepdata['h0']=ReadFixedEffect(folder+vessel+mouse+analysis+'PVS_'+'medianepisode_'+ftype)
      
    sleepdata['ampcard']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'cardiac_amp_'+ftype)
    sleepdata['periodcard']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'cardiac_period_'+ftype)

    sleepdata['ampresp']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'resp_amp_'+ftype)
    sleepdata['periodresp']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'resp_period_'+ftype)
    
    sleepdata['ampLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'LF_amp_'+ftype)
    sleepdata['periodLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'LF_period_'+ftype)    
    
    sleepdata['ampVLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'VLF_amp_'+ftype)
    sleepdata['periodVLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'VLF_period_'+ftype)   
      
    # get the names of the stage
    # for now just sleeping
    spanstages=sleepdata['Rv']
    
    spanhmem=[1e-4,2e-4]
    spanE=[2e4,5e4,10e4]
    spanK=[1e-14,1e-15]
    
    
    slurmfiles=[]
    for FB in ['card','resp','LF','VLF'] :
        for lpvs in spanlpvs:
            for hmem in spanhmem:
                for E in spanE :
                    for K in spanK :
                        for stage in spanstages :
                        
           
                            jobname='PVSBrain'+'-l%.0e'%lpvs+'-hmem%.0e'%hmem+'-E%.0f'%(E/1e4)+'-K%.0e'%K+'-'+stage+'-'+FB
                            print(jobname)
                            
                            #get properties
                            rv=sleepdata['Rv'][stage]*1e-4 #cm
                            h0=sleepdata['h0'][stage]*1e-4 #cm
                            fi=1/sleepdata['period'+FB][stage]
                            ai=sleepdata['amp'+FB][stage]/sleepdata['Rv'][stage] # ratio 
                            
                            # divide by two because the peak to peak amplitude as we use h0(1+ai sin ( w t))
                            ai=ai/2
                            
                            print('rv:',rv)
                            print('h0:',h0)
                            print('f:',fi)
                            print('a:',ai)
                            #estimate best time parameters
                            
    
                            tend=20
                            
                            # we would like 4 output per period
                            toutput=1/fi/4
                            
                            # we set dt as the toutput
                            dt=1/fi/20
                            
                            # output time for the flow computation
                            toutputcycle=max(1/fi/10,dt)
            
            
                            slurmfile=jobname+'.slurm'
                            
                            
                            #write the slurm file
                            write_PVSBrain_slurm(jobname, [fi],[ai],rv,h0,'"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile,Kmem=K,K=1e-13,E=E,Emem=E,hmem=hmem, lpvs=lpvs,d=d,dt=dt, toutput=toutput,toutputcycle=toutputcycle,tend=tend)                              
                            
                            #update the list of slurm files to be launched in batch
                            slurmfiles.append(slurmfile)


    #write the batch file
    with open(seriedir+'batch','w') as f :
        i=1
        for slurmfile in slurmfiles :
            f.write('sbatch '+slurmfile+' &\n')
            if not(i%100):
                f.write('sleep 1 \n')
            i+=1
    f.close()


    # Dispersion analaysis
    # --------------------
    # for each state we need mean lumen radius, mean PVS thickness + amp period x 4 
    # we compute each FB separatelly and the total of all
    
    
    # serie name
    serie='dispersionRandomWT9'
    
    # create a folder for the slurm files and the batch file
    if not os.path.exists(serie):
        os.makedirs(serie)
    seriedir=serie+'/' 
    

    
    spanVcard=[1e-4,10e-4,50e-4,100e-4]
     

       
    # awake data
    folder='/home/alexandra/Documents/Data/Celine/penetrating_arterioles_WT9_onlyWT_includingArea/'
    vessel='PenetratingArtrioles'
    mouse='WT9_'
    analysis='wake_'
    ftype='randomEffects.txt'
    
    
    awakedata={}
    awakedata['Rv']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'medianepisode_'+ftype)
    awakedata['h0']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'medianepisode_'+ftype)

    
    awakedata['ampcard']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'cardiac_amp_'+ftype)
    awakedata['ampresp']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'resp_amp_'+ftype)
    awakedata['ampLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'LF_amp_'+ftype)
    awakedata['ampVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'VLF_amp_'+ftype)

    awakedata['amp_endfootcard']=ReadRandomEffect(folder+vessel+mouse+analysis+'endfoot_'+'cardiac_amp_'+ftype)
    awakedata['amp_endfootresp']=ReadRandomEffect(folder+vessel+mouse+analysis+'endfoot_'+'resp_amp_'+ftype)
    awakedata['amp_endfootLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'endfoot_'+'LF_amp_'+ftype)
    awakedata['amp_endfootVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'endfoot_'+'VLF_amp_'+ftype)

    awakedata['amp_lumencard']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'cardiac_amp_'+ftype)
    awakedata['amp_lumenresp']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'resp_amp_'+ftype)
    awakedata['amp_lumenLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'LF_amp_'+ftype)
    awakedata['amp_lumenVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'VLF_amp_'+ftype)    


    awakedata['area']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'medianepisode_'+ftype)
    
    awakedata['amp_areacard']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'cardiac_amp_'+ftype)
    awakedata['amp_arearesp']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'resp_amp_'+ftype)
    awakedata['amp_areaLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'LF_amp_'+ftype)
    awakedata['amp_areaVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'VLF_amp_'+ftype)

    awakedata['periodcard']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'cardiac_period_'+ftype)
    awakedata['periodresp']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'resp_period_'+ftype)
    awakedata['periodLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'LF_period_'+ftype)
    awakedata['periodVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'VLF_period_'+ftype)
    
    
    # sleep data
    analysis='sleep_'

    
    sleepdata={}
    sleepdata['Rv']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'medianepisode_'+ftype)
    sleepdata['h0']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'medianepisode_'+ftype)
    
    sleepdata['ampcard']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'cardiac_amp_'+ftype)
    sleepdata['ampresp']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'resp_amp_'+ftype)
    sleepdata['ampLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'LF_amp_'+ftype)
    sleepdata['ampVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'VLF_amp_'+ftype)

    sleepdata['amp_endfootcard']=ReadRandomEffect(folder+vessel+mouse+analysis+'endfoot_'+'cardiac_amp_'+ftype)
    sleepdata['amp_endfootresp']=ReadRandomEffect(folder+vessel+mouse+analysis+'endfoot_'+'resp_amp_'+ftype)
    sleepdata['amp_endfootLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'endfoot_'+'LF_amp_'+ftype)
    sleepdata['amp_endfootVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'endfoot_'+'VLF_amp_'+ftype)

    sleepdata['amp_lumencard']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'cardiac_amp_'+ftype)
    sleepdata['amp_lumenresp']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'resp_amp_'+ftype)
    sleepdata['amp_lumenLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'LF_amp_'+ftype)
    sleepdata['amp_lumenVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'VLF_amp_'+ftype)   
    
    sleepdata['amp_areacard']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'cardiac_amp_'+ftype)
    sleepdata['amp_arearesp']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'resp_amp_'+ftype)
    sleepdata['amp_areaLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'LF_amp_'+ftype)
    sleepdata['amp_areaVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'VLF_amp_'+ftype)

    sleepdata['area']=ReadRandomEffect(folder+vessel+mouse+analysis+'Area_'+'medianepisode_'+ftype)

    sleepdata['periodcard']=ReadRandomEffect(folder+vessel+mouse+analysis+'lumen_'+'cardiac_period_'+ftype)
    sleepdata['periodresp']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'resp_period_'+ftype)
    sleepdata['periodLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'LF_period_'+ftype)
    sleepdata['periodVLF']=ReadRandomEffect(folder+vessel+mouse+analysis+'PVS_'+'VLF_period_'+ftype)
    
    # get the names of the stages
    # for now just sleeping
    spanstages=sleepdata['Rv'] 
    

    
                        
    # get the number of vessels
    numbervessels=len(sleepdata['Rv']['baseline'])
    
    def intersect(*d):
        result = set(d[0]).intersection(*d[1:])
        return result



    
    # functions to estimate the max velocity in the PVS :
    def A(t, a, f, phi=0, Rv0=8e-4, h0=2e-4) :
        w=2*np.pi*f
        Av0=np.pi*Rv0**2
        Aast0=np.pi*(Rv0+h0)**2
        A0=Aast0-Av0
        return A0*(1+a*np.sin(w*t+phi*2*np.pi))
    
    def dAdt(t, a, f, phi=0, Rv0=8e-4, h0=2e-4) :
        w=2*np.pi*f
        Av0=np.pi*Rv0**2
        Aast0=np.pi*(Rv0+h0)**2
        A0=Aast0-Av0
        return A0*(w*a*np.cos(w*t+phi*2*np.pi))
        
    def Q (s, t, a, f, l, phi=0, Rv0=8e-4, h0=2e-4) :
        return -dAdt(t, a, f, phi, Rv0,h0)*(s-l)

    def U (s, t, a, f, l,  phi=0, Rv0=8e-4, h0=2e-4) :
        return Q(s, t, a, f, l, phi, Rv0, h0)/A ( t, a, f, phi, Rv0, h0)

    
    amplituderandom=[]

    for lpvs in spanlpvs:
        for d in spandiffusion :
            
            nl=int(lpvs/1e-4)
            nr=6
            sigma=2e-4
            
            # serie name
            serie='dispersionRandomWT9t40area'+'-d%.0e'%d+'-l%.0e'%lpvs
    
            # create a folder for the slurm files and the batch file
            if not os.path.exists(serie):
                os.makedirs(serie)
                seriedir=serie+'/'
    
            import time

            slurmfiles=[]
            for stage in spanstages:
                
                    
                ### We generale area deformation for the cardiac FB in order to impose the velocity
                    
                # treat the cardiac time scale
                # We assume the value of the velocity due to cardiac pulsation
                for vcard in spanVcard:
                    #Umax = a w L . we fix L and take w from measurement. 
                    #It gives us the amplitude of deformation of area to impose.
                    
                    #get the vessel IDs
                    vesselid_Rv = sleepdata['Rv'][stage].keys()
                    vesselid_h0 = sleepdata['h0'][stage].keys()

                    vesselid_osc = sleepdata['amp_areacard'][stage].keys()


                    vesselIDs=intersect(vesselid_Rv,vesselid_h0,vesselid_osc) 
                   

                
                    for vesselID in vesselIDs :
                
                    
                        jobname='disp'+'-d%.0e'%d+'-l%.0e'%lpvs+'-'+stage+'-'+'card'+'-v%.0e'%vcard+'-id'+vesselID
                        print(jobname)
                        
                        #get properties
                        rv=sleepdata['Rv'][stage][vesselID]*1e-4 #cm
                        h0=sleepdata['h0'][stage][vesselID]*1e-4 #cm
                        rpvs=rv+h0
                        fi=1/sleepdata['period'+'card'][stage][vesselID]
                        
                        
                        wi=2*np.pi*fi
                        
                        ampArea=vcard/wi/lpvs
                
                       
                        ai=ampArea
                        
                        print('rv:',rv)
                        print('h0:',h0)
                        print('f:',fi)
                        print('a:',ai)
                        

                        #estimate best time parameters
                        
                        #the end of the simulation should at least cover 5 periods (to get a linear fit)
                        # and the time to diffuse in the transveral direction
                        #tend=max(5/fi,2*h0**2/d)
                        
                        tend=20
                
                        # we would like 4 output per period
                        toutput=1/fi*4
                        
                        # we set dt as the toutput
                        dt=toutput
                        
                        # we would like at least 1000 time steps
                        while dt>(tend/1000) :
                            dt/=2
                
                        # we would like at max dt=5e-3
                        while dt>(5e-3) :
                            dt/=2
                        
                        #and at least 100 output over the whole simulation
                        while tend/toutput <100 :
                            toutput/=2
                
                        Noutput=tend/toutput
                
                        #estiamate of the max velocity
                        Umax=U(0, 0, ai, fi, lpvs, Rv0=rv, h0=h0)
                        #estimate of the Peclet number
                        Pe=h0*Umax/d/2
                        # constraining velocity for the CFL condition
                        uconstrain=max(d/h0,Umax)

                        dx=h0/nr
                        
                        CFL=(uconstrain/(dx/dt))
                        while CFL>0.7 :
                            dt/=2
                            CFL=(uconstrain/(dx/dt))
                            
                        print('# CFL :' ,CFL)
                
                        if  True:
                            print('# Pe : %e'%Pe)
                            print('# n time steps : %i'%(tend/dt))
                            print('# n output : %i'%(Noutput))
                            print('# n step/output : %f'%(toutput/dt))
                            print('# dt : %5e'%dt)
                
                            if Noutput>500 :
                                print('f',f)
                                print('tend',tend)
                                print('dt',dt)
                                print('N',Noutput)
                                print(Noutput)
                                print('aie trop de output!\n')
                            if int(toutput/dt)== 0 :
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if dt>toutput:
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if dt>=tend:
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if toutput>=tend:
                                print('tend',tend)
                                print('toutput mauvais t output',toutput)
                                print('aie !\n')  
                        
                            print('\n') 
                        
                        # slurmfile=jobname+'.slurm'
                        
                        # #write the slurm file
                        # #gaussian analysis
                        # write_state_slurm(jobname, [fi],[ai],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='gaussian', c0valuePVS=1, c0valueSAS=0, sigma=sigma, sasbc='scenarioA', refineleft=False)                              
                        
                        # #intake
                        # #write_state_slurm(jobname, [fi],[ai],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=200, nr=nr,c0init='uniform', c0valuePVS=0, c0valueSAS=1, sigma=1e-4, sasbc='scenarioE', refineleft=False, sas=False)                              
                  
                        # #update the list of slurm files to be launched in batch
                        # slurmfiles.append(slurmfile)
                        
                        
                # then we treat the measure values
            
                # each FB separately
                for FB in ['LF','VLF'] :

                    #get the vessel ID present for all freq
                    vesselid_Rv = sleepdata['Rv'][stage].keys()
                    vesselid_h0 = sleepdata['h0'][stage].keys()

                    vesselid_osc = sleepdata['amp_area'+FB][stage].keys()


                    vesselIDs=intersect(vesselid_Rv,vesselid_h0,vesselid_osc)  
                   
                
                    for vesselID in vesselIDs :
                    
                        jobname='disp'+'-d%.0e'%d+'-l%.0e'%lpvs+'-'+stage+'-'+FB+'-id'+vesselID
                        print(jobname)
                        
                        #get properties
                        rv=sleepdata['Rv'][stage][vesselID]*1e-4 #cm
                        h0=sleepdata['h0'][stage][vesselID]*1e-4 #cm
                        rpvs=rv+h0
                        fi=1/sleepdata['period'+FB][stage][vesselID]
                        
                        meanarea=sleepdata['area'][stage][vesselID]
                    
                    
                        amp_pvs=sleepdata['amp'+FB][stage][vesselID]*1e-4/2
                        amp_endfoot=sleepdata['amp_endfoot'+FB][stage][vesselID]*1e-4/2
                        amp_lumen=sleepdata['amp_lumen'+FB][stage][vesselID]*1e-4/2
                        
                        fi=1/sleepdata['period'+FB][stage][vesselID]
                        
                        
                        
                        # Compute area change from PVS thickness oscillations
                        # A0=np.pi*(rpvs)**2- np.pi*(rv)**2
                        # # compute the change of area
                        # #This doesnt work because we can get negative amplitude. 
                        # #This is because we should not use estimates of radius to compute area
                        # # We would need a model of the area ampliture
                        
                        # #Amin=np.pi*(rpvs+amp_endfoot)**2- np.pi*(rv+amp_lumen)**2
                        # #Amax=np.pi*(rpvs-amp_endfoot)**2- np.pi*(rv-amp_lumen)**2
                        
                        # # So what we do is to fix Rpvs and compute area change using the change of thickness
                        # Amin=np.pi*(rpvs)**2- np.pi*(rv+amp_pvs)**2
                        # Amax=np.pi*(rpvs)**2- np.pi*(rv-amp_pvs)**2        
                        
                        #if Amin>Amax : 
                        #    print('Problem in estimating the area !')
                        #    stop()  
                        # ai=(Amax-Amin)/A0/2
                        
                        
                        
                        # Compute area change directly from area oscilations
                        ai=sleepdata['amp_area'+FB][stage][vesselID]/meanarea
                        ai/=2
                        
                        amplituderandom.append(ai)
                        
                          
                        
                        print('stage',stage)
                        print('vessel',vesselID)
                        print('FB',FB)
                        print('rpvs:',rpvs)
                        print('rv:',rv)
                        
                        print('arpvs:',amp_endfoot)
                        print('arv:',amp_lumen)
                        
                        print('h0:',h0)
                        print('f:',fi)
                        print('a:',ai)
    
                                          
    
                        #estimate best time parameters
                        
                        #the end of the simulation should at least cover 3 periods (to get a linear fit)
                        # and the time to diffuse in the transveral direction
                        #tend=max(5/fi,2*h0**2/d)
                        
                        tend=40

                        # we would like 4 output per period
                        toutput=1/fi*4
                        
                        # we set dt as the toutput
                        dt=toutput
                        
                        # we would like at least 1000 time steps
                        while dt>(tend/1000) :
                            dt/=2

                        # we would like at max dt=5e-3
                        while dt>(5e-3) :
                            dt/=2
                        
                        #and at least 100 output over the whole simulation
                        while tend/toutput <100 :
                            toutput/=2
            
                        Noutput=tend/toutput
        
                        #estiamate of the max velocity
                        Umax=U(0, 0, ai, fi, lpvs, Rv0=rv, h0=h0)
                        #estimate of the Peclet number
                        Pe=h0*Umax/d/2
                        # constraining velocity for the CFL condition
                        uconstrain=max(d/h0,Umax)
                        # number of cells in transversal direction
                        nr=6
                        dx=h0/nr
                        
                        CFL=(uconstrain/(dx/dt))
                        while CFL>0.7 :
                            dt/=2
                            CFL=(uconstrain/(dx/dt))
                            
                        print('# CFL :' ,CFL)

                        if  True:
                            print('# Pe : %e'%Pe)
                            print('# n time steps : %i'%(tend/dt))
                            print('# n output : %i'%(Noutput))
                            print('# n step/output : %f'%(toutput/dt))
                            print('# dt : %5e'%dt)

                            if Noutput>500 :
                                print('f',f)
                                print('tend',tend)
                                print('dt',dt)
                                print('N',Noutput)
                                print(Noutput)
                                print('aie trop de output!\n')
                            if int(toutput/dt)== 0 :
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if dt>toutput:
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if dt>=tend:
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if toutput>=tend:
                                print('tend',tend)
                                print('toutput mauvais t output',toutput)
                                print('aie !\n')  
                        
                            print('\n') 
                        
                        
                        
                        
                        
                        slurmfile=jobname+'.slurm'

                        
                        #write the slurm file
                        # gaussian analysis
                        write_state_slurm(jobname, [fi],[ai],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='gaussian', c0valuePVS=1, c0valueSAS=0, sigma=sigma, sasbc='scenarioA', refineleft=False)                              
                        
                        # intake
                        #write_state_slurm(jobname, [fi],[ai],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=200, nr=nr,c0init='uniform', c0valuePVS=0, c0valueSAS=1, sigma=1e-4, sasbc='scenarioE', refineleft=False, sas=False)                              

                        #update the list of slurm files to be launched in batch
                        slurmfiles.append(slurmfile)
                        
                    # superposition of LF anf VLF
                    
                    
                    #get the vessel ID present for all freq
                    vesselid_Rv = sleepdata['Rv'][stage].keys()
                    vesselid_h0 = sleepdata['h0'][stage].keys()

                    vesselid_oscLF = sleepdata['amp_area'+'LF'][stage].keys()
                    vesselid_oscVLF = sleepdata['amp_area'+'VLF'][stage].keys()


                    vesselIDs=intersect(vesselid_Rv,vesselid_h0,vesselid_oscLF,vesselid_oscVLF)  
                   
                
                    for vesselID in vesselIDs :
                    
                        jobname='disp'+'-d%.0e'%d+'-l%.0e'%lpvs+'-'+stage+'-'+'LFVLF'+'-id'+vesselID
                        print(jobname)
                        
                        #get properties
                        rv=sleepdata['Rv'][stage][vesselID]*1e-4 #cm
                        h0=sleepdata['h0'][stage][vesselID]*1e-4 #cm
                        rpvs=rv+h0
                        fi=1/sleepdata['period'+FB][stage][vesselID]
                        
                        meanarea=sleepdata['area'][stage][vesselID]
                    
                    
                       
                        fVLF=1/sleepdata['period'+'VLF'][stage][vesselID]
                        fLF=1/sleepdata['period'+'LF'][stage][vesselID]
                        
                        
                        # Compute area change directly from area oscilations
                        aVLF=sleepdata['amp_area'+'VLF'][stage][vesselID]/meanarea/2
                        aLF=sleepdata['amp_area'+'LF'][stage][vesselID]/meanarea/2
                        
                        
                          
                        
                        print('stage',stage)
                        print('vessel',vesselID)
                        print('FB',FB)
                        print('rpvs:',rpvs)
                        print('rv:',rv)
                        
                        print('arpvs:',amp_endfoot)
                        print('arv:',amp_lumen)
                        
                        print('h0:',h0)
                        print('f:',[fVLF,fLF])
                        print('a:',[aVLF,aLF])
    
                                          
    
                        #estimate best time parameters
                        
                        #the end of the simulation should at least cover 3 periods (to get a linear fit)
                        # and the time to diffuse in the transveral direction
                        #tend=max(5/fi,2*h0**2/d)
                        
                        tend=20

                        # we would like 4 output per period
                        toutput=1/fLF*4
                        
                        # we set dt as the toutput
                        dt=toutput
                        
                        # we would like at least 1000 time steps
                        while dt>(tend/1000) :
                            dt/=2

                        # we would like at max dt=5e-3
                        while dt>(5e-3) :
                            dt/=2
                        
                        #and at least 100 output over the whole simulation
                        while tend/toutput <100 :
                            toutput/=2
            
                        Noutput=tend/toutput
        
                        #estiamate of the max velocity
                        Umax=U(0, 0, aLF+aVLF, fLF, lpvs, Rv0=rv, h0=h0)
                        #estimate of the Peclet number
                        Pe=h0*Umax/d/2
                        # constraining velocity for the CFL condition
                        uconstrain=max(d/h0,Umax)
                        # number of cells in transversal direction
                        nr=6
                        dx=h0/nr
                        
                        CFL=(uconstrain/(dx/dt))
                        while CFL>0.7 :
                            dt/=2
                            CFL=(uconstrain/(dx/dt))
                            
                        print('# CFL :' ,CFL)

                        if  True:
                            print('# Pe : %e'%Pe)
                            print('# n time steps : %i'%(tend/dt))
                            print('# n output : %i'%(Noutput))
                            print('# n step/output : %f'%(toutput/dt))
                            print('# dt : %5e'%dt)

                            if Noutput>500 :
                                print('f',f)
                                print('tend',tend)
                                print('dt',dt)
                                print('N',Noutput)
                                print(Noutput)
                                print('aie trop de output!\n')
                            if int(toutput/dt)== 0 :
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if dt>toutput:
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if dt>=tend:
                                print('dt',tend)
                                print('toutput',toutput)
                                print('aie mauvais dt!\n')
                            if toutput>=tend:
                                print('tend',tend)
                                print('toutput mauvais t output',toutput)
                                print('aie !\n')  
                        
                            print('\n') 
                        
                        
                        
                        
                        
                        slurmfile=jobname+'.slurm'

                        
                        #write the slurm file
                        # gaussian analysis
                        write_state_slurm(jobname, [fVLF,fLF],[aVLF,aLF],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='gaussian', c0valuePVS=1, c0valueSAS=0, sigma=sigma, sasbc='scenarioA', refineleft=False)                              
                        
                        # intake
                        #write_state_slurm(jobname, [fVLF fLF],[aVLF aLF],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=200, nr=nr,c0init='uniform', c0valuePVS=0, c0valueSAS=1, sigma=1e-4, sasbc='scenarioE', refineleft=False, sas=False)                              

                        #update the list of slurm files to be launched in batch
                        slurmfiles.append(slurmfile)
                        
                        
                    # and supperposition of FB : todo
                    

            #write the batch file
            with open(seriedir+'batch','w') as f :
                i=1
                for slurmfile in slurmfiles :
                    f.write('sbatch '+slurmfile+' &\n')
                    if not(i%100):
                        f.write('sleep 1 \n')
                    i+=1
            f.close()
    
    
    #### Intake analysis over just one stage

    for lpvs in spanlpvs:
        for d in spandiffusion :
            
            nl=int(lpvs/1e-4)
            nr=6
            sigma=2e-4
            
            # serie name
            serie='intakeRandomWT9t500area'+'-d%.0e'%d+'-l%.0e'%lpvs
    
            # create a folder for the slurm files and the batch file
            if not os.path.exists(serie):
                os.makedirs(serie)
                seriedir=serie+'/'
    
            import time
            
            slurmfiles=[]
            # diffusion only
            jobname='diffusion'+'-d%.0e'%d+'-l%.0e'%lpvs
            print(jobname)
                
            #get properties
            rv=np.median(list(sleepdata['Rv']['baseline'].values()))*1e-4 #cm
            h0=np.median(list(sleepdata['h0']['baseline'].values()))*1e-4 #cm
            rpvs=rv+h0

            
            
                        
            tend=500

            toutput=0.5
                        
            dt=5e-2
                        
                        
            slurmfile=jobname+'.slurm'

            #write the slurm file
            # gaussian analysis
            #write_state_slurm(jobname, [0],[0],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='gaussian', c0valuePVS=1, c0valueSAS=0, sigma=sigma, sasbc='scenarioA', refineleft=False)                              
                        
            # intake
            write_state_slurm(jobname, [0],[0],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='uniform', c0valuePVS=0, c0valueSAS=1, sigma=sigma, sasbc='scenarioE', refineleft=False, sas=False)                              

            #update the list of slurm files to be launched in batch
            slurmfiles.append(slurmfile)
                        
            # superposition of LF anf VLF
                    
            for stage in spanstages :
                    #get the vessel ID present for all freq
                    vesselid_Rv = sleepdata['Rv'][stage].keys()
                    vesselid_h0 = sleepdata['h0'][stage].keys()

                    vesselid_oscLF = sleepdata['amp_area'+'LF'][stage].keys()
                    vesselid_oscVLF = sleepdata['amp_area'+'VLF'][stage].keys()


                    vesselIDs=intersect(vesselid_Rv,vesselid_h0,vesselid_oscLF,vesselid_oscVLF)  
                   
                
                    for vesselID in vesselIDs :
                    
                        jobname='disp'+'-d%.0e'%d+'-l%.0e'%lpvs+'-'+stage+'-'+'LFVLF'+'-id'+vesselID
                        print(jobname)
                        
                        #get properties
                        rv=sleepdata['Rv'][stage][vesselID]*1e-4 #cm
                        h0=sleepdata['h0'][stage][vesselID]*1e-4 #cm
                        rpvs=rv+h0
                        
                        meanarea=sleepdata['area'][stage][vesselID]
                    
                    
                       
                        fVLF=1/sleepdata['period'+'VLF'][stage][vesselID]
                        fLF=1/sleepdata['period'+'LF'][stage][vesselID]
                        
                        
                        # Compute area change directly from area oscilations
                        aVLF=sleepdata['amp_area'+'VLF'][stage][vesselID]/meanarea/2
                        aLF=sleepdata['amp_area'+'LF'][stage][vesselID]/meanarea/2
                        
                        
                          
                        
                        print('stage',stage)
                        print('vessel',vesselID)
                        print('FB',FB)
                        print('rpvs:',rpvs)
                        print('rv:',rv)
                        
                        print('arpvs:',amp_endfoot)
                        print('arv:',amp_lumen)
                        
                        print('h0:',h0)
                        print('f:',[fVLF,fLF])
                        print('a:',[aVLF,aLF])
    
                                          
    
                        #estimate best time parameters
                        
                        #the end of the simulation should at least cover 3 periods (to get a linear fit)
                        # and the time to diffuse in the transveral direction
                        #tend=max(5/fi,2*h0**2/d)
                        
                        tend=500

                        # we would like 4 output per period
                        toutput=0.5
                        
                        # we set dt as the toutput
                        dt=5e-2
                        
            
                        Noutput=tend/toutput
        
                        #estiamate of the max velocity
                        Umax=U(0, 0, aLF+aVLF, fLF, lpvs, Rv0=rv, h0=h0)
                        #estimate of the Peclet number
                        Pe=h0*Umax/d/2
                        # constraining velocity for the CFL condition
                        uconstrain=max(d/h0,Umax)
                        # number of cells in transversal direction
                        dx=h0/nr
                        
                        CFL=(uconstrain/(dx/dt))
                        while CFL>0.7 :
                            dt/=2
                            CFL=(uconstrain/(dx/dt))
                            
                        print('# CFL :' ,CFL)


                        
                        
                        
                        slurmfile=jobname+'.slurm'

                        
                        #write the slurm file
                        # gaussian analysis
                        #write_state_slurm(jobname, [fVLF,fLF],[aVLF,aLF],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='gaussian', c0valuePVS=1, c0valueSAS=0, sigma=sigma, sasbc='scenarioA', refineleft=False)                              
                        
                        # intake
                        write_state_slurm(jobname, [fVLF, fLF],[aVLF, aLF],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='uniform', c0valuePVS=0, c0valueSAS=1, sigma=sigma, sasbc='scenarioE', refineleft=False, sas=False)                              

                        #update the list of slurm files to be launched in batch
                        slurmfiles.append(slurmfile)
                        
                        
                    # and supperposition of FB : todo
                    

            #write the batch file
            with open(seriedir+'batch','w') as f :
                i=1
                for slurmfile in slurmfiles :
                    f.write('sbatch '+slurmfile+' &\n')
                    if not(i%100):
                        f.write('sleep 1 \n')
                    i+=1
            f.close()
    
    ######
    ## Fixed effect analysis 
    ##########################3
    
    
    ## Dispersion analysis per state over the full population
    # sleep data
    folder='/home/alexandra/Documents/Data/Celine/penetrating_arterioles_WT9_onlyWT_includingArea/'
    
    vessel='PenetratingArtrioles'
    
    mouse='WT9_'
    
    analysis='sleep_'
    ftype='fixedEffects.txt'
      
    sleepdata={}
    sleepdata['Rv']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'medianepisode_'+ftype)
    sleepdata['h0']=ReadFixedEffect(folder+vessel+mouse+analysis+'PVS_'+'medianepisode_'+ftype)
    sleepdata['area']=ReadFixedEffect(folder+vessel+mouse+analysis+'Area_'+'medianepisode_'+ftype)
    
    sleepdata['amp_endfootcard']=ReadFixedEffect(folder+vessel+mouse+analysis+'endfoot_'+'cardiac_amp_'+ftype)
    sleepdata['amp_lumencard']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'cardiac_amp_'+ftype)
    sleepdata['ampcard']=ReadFixedEffect(folder+vessel+mouse+analysis+'PVS_'+'cardiac_amp_'+ftype)
    sleepdata['amp_areacard']=ReadFixedEffect(folder+vessel+mouse+analysis+'Area_'+'cardiac_amp_'+ftype)
    sleepdata['periodcard']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'cardiac_period_'+ftype)
    
    
    sleepdata['amp_endfootresp']=ReadFixedEffect(folder+vessel+mouse+analysis+'endfoot_'+'resp_amp_'+ftype)
    sleepdata['amp_lumenresp']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'resp_amp_'+ftype)
    sleepdata['ampresp']=ReadFixedEffect(folder+vessel+mouse+analysis+'PVS_'+'resp_amp_'+ftype)
    sleepdata['amp_arearesp']=ReadFixedEffect(folder+vessel+mouse+analysis+'Area_'+'resp_amp_'+ftype)
    sleepdata['periodresp']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'resp_period_'+ftype)
    
    sleepdata['amp_endfootLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'endfoot_'+'LF_amp_'+ftype)
    sleepdata['amp_lumenLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'LF_amp_'+ftype)
    sleepdata['ampLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'PVS_'+'LF_amp_'+ftype)
    sleepdata['amp_areaLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'Area_'+'LF_amp_'+ftype)
    sleepdata['periodLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'LF_period_'+ftype)    
    
    
    sleepdata['amp_endfootVLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'endfoot_'+'VLF_amp_'+ftype)
    sleepdata['amp_lumenVLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'VLF_amp_'+ftype)
    sleepdata['ampVLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'PVS_'+'VLF_amp_'+ftype)
    sleepdata['amp_areaVLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'Area_'+'VLF_amp_'+ftype)
    sleepdata['periodVLF']=ReadFixedEffect(folder+vessel+mouse+analysis+'lumen_'+'VLF_period_'+ftype) 
    

    spanstages=sleepdata['Rv']
    
    areachange=[]
    areachangeth=[]
    
    for lpvs in spanlpvs :
        for d in spandiffusion:
            
            nl=int(lpvs/1e-4)
            nr=8
            sigma=2e-4


            
            # serie name
            serie='dispersionFixedWT9t20area'+'-d%.0e'%d+'-l%.0e'%lpvs
    
            # create a folder for the slurm files and the batch file
            if not os.path.exists(serie):
                os.makedirs(serie)
                seriedir=serie+'/'
    
            
            velocityDict={}
            slurmfiles=[]
            
            for stage in spanstages :
                velocityDict[stage]={}
                
                
                ### We generale area deformation for the cardiac FB in order to impose the velocity
                
                # treat the cardiac time scale
                # We assume the value of the velocity due to cardiac pulsation
                for vcard in spanVcard:
                    #Umax = a w L . we fix L and take w from measurement. 
                    #It gives us the amplitude of deformation of area to impose.
                    
                    jobname='disp'+'-d%.0e'%d+'-l%.0e'%lpvs+'-'+stage+'-'+'card'+'-v%.0e'%vcard
                    print(jobname)
                    
                    #get properties
                    rv=sleepdata['Rv'][stage]*1e-4 #cm
                    h0=sleepdata['h0'][stage]*1e-4 #cm
                    fi=1/sleepdata['period'+'card'][stage]
                    
                    
                    wi=2*np.pi*fi
                    
                    ampArea=vcard/wi/lpvs
            
                   
                    ai=ampArea
                    
                    print('rv:',rv)
                    print('h0:',h0)
                    print('f:',fi)
                    print('a:',ai)
                    #estimate best time parameters
                    
                    #the end of the simulation should at least cover 5 periods (to get a linear fit)
                    # and the time to diffuse in the transveral direction
                    #tend=max(5/fi,2*h0**2/d*4)
                    
                    tend=20
            
                    # we would like 4 output per period
                    toutput=1/fi*4
                    
                    # we set dt as the toutput
                    dt=toutput
                    
                    # we would like at least 1000 time steps
                    while dt>(tend/1000) :
                        dt/=2
            
                    # we would like at max dt=5e-3
                    while dt>(5e-3) :
                        dt/=2
                    
                    #and at least 100 output over the whole simulation
                    while tend/toutput <100 :
                        toutput/=2
            
                    Noutput=tend/toutput
            
                    #estiamate of the max velocity
                    Umax=U(0, 0, ai, fi, lpvs, Rv0=rv, h0=h0)
                    #estimate of the Peclet number
                    Pe=h0*Umax/d/2
                    # constraining velocity for the CFL condition
                    uconstrain=max(d/h0,Umax)

                    dx=h0/nr
                    
                    CFL=(uconstrain/(dx/dt))
                    while CFL>0.7 :
                        dt/=2
                        CFL=(uconstrain/(dx/dt))
                        
                    print('# CFL :' ,CFL)
            
                    if  True:
                        print('# Pe : %e'%Pe)
                        print('# n time steps : %i'%(tend/dt))
                        print('# n output : %i'%(Noutput))
                        print('# n step/output : %f'%(toutput/dt))
                        print('# dt : %5e'%dt)
            
                        if Noutput>500 :
                            print('f',f)
                            print('tend',tend)
                            print('dt',dt)
                            print('N',Noutput)
                            print(Noutput)
                            print('aie trop de output!\n')
                        if int(toutput/dt)== 0 :
                            print('dt',tend)
                            print('toutput',toutput)
                            print('aie mauvais dt!\n')
                        if dt>toutput:
                            print('dt',tend)
                            print('toutput',toutput)
                            print('aie mauvais dt!\n')
                        if dt>=tend:
                            print('dt',tend)
                            print('toutput',toutput)
                            print('aie mauvais dt!\n')
                        if toutput>=tend:
                            print('tend',tend)
                            print('toutput mauvais t output',toutput)
                            print('aie !\n')  
                    
                        print('\n') 
                    
                    slurmfile=jobname+'.slurm'
                    
                    #write the slurm file
                    #gaussian analysis
                    write_state_slurm(jobname, [fi],[ai],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='gaussian', c0valuePVS=1, c0valueSAS=0, sigma=sigma, sasbc='scenarioA', refineleft=False)                              
                    
                    #intake analysis
                    #write_state_slurm(jobname, [fi],[ai],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=200, nr=nr,c0init='uniform', c0valuePVS=0, c0valueSAS=1, sigma=1e-4, sasbc='scenarioE', refineleft=False, sas=False)                              
              
                    #update the list of slurm files to be launched in batch
                    slurmfiles.append(slurmfile)
                    
                    
                # then we treat the measure values
                    
                for FB in ['LF','VLF'] :
                    
                    jobname='disp'+'-d%.0e'%d+'-l%.0e'%lpvs+'-'+stage+'-'+FB
                    print(jobname)
                    
                    #get properties
                    rv=sleepdata['Rv'][stage]*1e-4 #cm
                    h0=sleepdata['h0'][stage]*1e-4 #cm
                    rpvs=rv+h0
                    
                    meanarea=sleepdata['area'][stage]
                    
                    
                    amp_pvs=sleepdata['amp'+FB][stage]*1e-4/2
                    amp_endfoot=sleepdata['amp_endfoot'+FB][stage]*1e-4/2
                    amp_lumen=sleepdata['amp_lumen'+FB][stage]*1e-4/2
                    
                    fi=1/sleepdata['period'+FB][stage]
                    
                    
                    
                    # Compute area change from PVS thickness oscillations
                    A0=np.pi*(rpvs)**2- np.pi*(rv)**2
                    # compute the change of area
                    #This doesnt work because we can get negative amplitude. 
                    #This is because we should not use estimates of radius to compute area
                    # We would need a model of the area ampliture
                    
                    #Amin=np.pi*(rpvs+amp_endfoot)**2- np.pi*(rv+amp_lumen)**2
                    #Amax=np.pi*(rpvs-amp_endfoot)**2- np.pi*(rv-amp_lumen)**2
                    
                    # So what we do is to fix Rpvs and compute area change using the change of thickness
                    Amin=np.pi*(rpvs)**2- np.pi*(rv+amp_pvs)**2
                    Amax=np.pi*(rpvs)**2- np.pi*(rv-amp_pvs)**2        
                    
                    
                    ai=(Amax-Amin)/A0/2
                    
                    areachangeth.append(ai)
                    
                    
                    # Compute area change directly from area oscilations
                    ai=sleepdata['amp_area'+FB][stage]/meanarea
                    ai/=2
                    
                    areachange.append(ai)
                    
                    Umax=U(0, 0, ai, fi, lpvs, Rv0=rv, h0=h0)
                    
                    velocityDict[stage][FB]={'Rv':rv,'h0':h0,'ai':ai,'fi':fi,'Umax':Umax}
                    print('stage',stage)
                    print('FB',FB)
                    print('rpvs:',rpvs)
                    print('rv:',rv)
                    
                    print('arpvs:',amp_endfoot)
                    print('arv:',amp_lumen)
                    
                    print('h0:',h0)
                    print('f:',fi)
                    print('a:',ai)
            
                    print('Umax',Umax)
                    print('')
                    
            
                    #estimate best time parameters
                    
                    #the end of the simulation should at least cover 5 periods (to get a linear fit)
                    # and the time to diffuse in the transveral direction
                    #tend=max(5/fi,2*h0**2/d*4) # for long vessel we can look at longer times
                    
                    tend=20
            
                    # we would like 4 output per period
                    toutput=1/fi*4
                    
                    # we set dt as the toutput
                    dt=toutput
                    
                    # we would like at least 1000 time steps
                    while dt>(tend/1000) :
                        dt/=2
            
                    # we would like at max dt=5e-3
                    while dt>(5e-3) :
                        dt/=2
                    
                    #and at least 100 output over the whole simulation
                    while tend/toutput <100 :
                        toutput/=2
            
                    Noutput=tend/toutput
            
                    #estiamate of the max velocity
                    Umax=U(0, 0, ai, fi, lpvs, Rv0=rv, h0=h0)
                    #estimate of the Peclet number
                    Pe=h0*Umax/d/2
                    # constraining velocity for the CFL condition
                    uconstrain=max(d/h0,Umax)
                    # number of cells in transversal direction
                    nr=6
                    dx=h0/nr
                    
                    CFL=(uconstrain/(dx/dt))
                    while CFL>0.7 :
                        dt/=2
                        CFL=(uconstrain/(dx/dt))
                        
                    print('# CFL :' ,CFL)
            
                    if  True:
                        print('# Pe : %e'%Pe)
                        print('# n time steps : %i'%(tend/dt))
                        print('# n output : %i'%(Noutput))
                        print('# n step/output : %f'%(toutput/dt))
                        print('# dt : %5e'%dt)
            
                        if Noutput>500 :
                            print('f',f)
                            print('tend',tend)
                            print('dt',dt)
                            print('N',Noutput)
                            print(Noutput)
                            print('aie trop de output!\n')
                        if int(toutput/dt)== 0 :
                            print('dt',tend)
                            print('toutput',toutput)
                            print('aie mauvais dt!\n')
                        if dt>toutput:
                            print('dt',tend)
                            print('toutput',toutput)
                            print('aie mauvais dt!\n')
                        if dt>=tend:
                            print('dt',tend)
                            print('toutput',toutput)
                            print('aie mauvais dt!\n')
                        if toutput>=tend:
                            print('tend',tend)
                            print('toutput mauvais t output',toutput)
                            print('aie !\n')  
                    
                        print('\n') 
                    
                    slurmfile=jobname+'.slurm'
                    
                    #write the slurm file
                    #gaussian analysis
                    write_state_slurm(jobname, [fi],[ai],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=nl, nr=nr,c0init='gaussian', c0valuePVS=1, c0valueSAS=0, sigma=sigma, sasbc='scenarioA', refineleft=False)                              
                    
                    #intake
                    #write_state_slurm(jobname, [fi],[ai],rv,h0, '"${USERWORK}/sleepoutput/'+serie+'"',seriedir+slurmfile, lpvs=lpvs,d=d,dt=dt, toutput=toutput,tend=tend,nl=200, nr=nr,c0init='uniform', c0valuePVS=0, c0valueSAS=1, sigma=1e-4, sasbc='scenarioE', refineleft=False, sas=False)                              
                      
                    #update the list of slurm files to be launched in batch
                    slurmfiles.append(slurmfile)
                
                
            #write the batch file
            with open(seriedir+'batch','w') as f :
                i=1
                for slurmfile in slurmfiles :
                    f.write('sbatch '+slurmfile+' &\n')
                    if not(i%100):
                        f.write('sleep 1 \n')
                    i+=1
            f.close()
            
            
            

    
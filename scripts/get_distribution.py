#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 16:45:48 2021

@author: alexandra
"""

import scipy.stats
import matplotlib.pyplot as plt
import scipy as sp
import numpy as np
import pandas as pd

import pyDOE



def timeparams(f,h0,D) :

    tend=max(3/f,2*h0**2/D)
    dt=1/f/8
    toutput=1/f/4

    while tend/dt <100 :
            dt/=2      
            
    while tend/toutput <100 :
        toutput/=2
            
    Noutput=tend/toutput
    
    if Noutput>500 :
        print('WARNING, more than 500 outputs ! N=%i'%Noutput)
        
    return ' -tend %e --time_step %e -toutput %e'%(tend,dt,toutput)

mouse='WT08'
study='03'
D=2e-7

markdown=[]

simucall='python3 ../fbb_DD/PVS_simulation.py'
diffusion=' --diffusion_coef %e'%D
default=' --resistance -1 -xi 100e-4  -lpvs 200e-4 -nr 8 --sigma 1e-4'
        


line1='| $R_v$    | Lumen radius (um)   | Lognormale   | '
line2='| $h_0$    | PVS thickness (um)   | Lognormale   | '
line3={}
line4={}
for bandname in ['cardiac','resp','LF','VLF']:                
    line3[bandname]='| $a$    | Amplitude (um)   | Lognormale   | '
    line4[bandname]='| $f$    | Frequency (Hz)   | Lognormale   | ' 
    
    
    
linescan='lumen'
datalumen=pd.read_pickle('../output/databases/amplitude_'+mouse+'-'+study+'-'+linescan+'.pkl')

linescan='endfoot'
dataendfoot=pd.read_pickle('../output/databases/amplitude_'+mouse+'-'+study+'-'+linescan+'.pkl')

linescan='PVS'
dataPVS=pd.read_pickle('../output/databases/amplitude_'+mouse+'-'+study+'-'+linescan+'.pkl')


# generate points of simulation

### Latin hypercube space generation
# Number of variables
# Rv, h0, amp, freq
Nva=4
# Number of simulation points to generate
N=15



figno=1

f=open('/home/alexandra/Documents/Python/sleep/sleep/scripts/script_'+mouse+study+'.sh', 'w')
    


for stage in ['REM','NREM','IS']:
    
    # Latin hypercube space
    LHS=pyDOE.lhs(Nva,N, criterion='center')
    
    
    filtre=(datalumen['stage']==stage)&(datalumen['bandname']=='LF')
    samp=datalumen[filtre]['mean']
    param=sp.stats.lognorm.fit(samp, floc=0) # fit the sample data
    
    x=np.linspace(max(0,np.mean(samp)-3*np.std(samp)),np.mean(samp)+3*np.std(samp),100)
    pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=0, scale=param[2]) # fitted distribution
    Rv=scipy.stats.lognorm.ppf(LHS[:,0], param[0], loc=0, scale=param[2])
    line1+=(' %.1e (%.1e) |'% (param[2],param[0]))  
    
    Rvmean=param[2]
    
    plt.figure(figno)
    plt.plot(x,pdf_fitted,'k-',label='fitted distribution')
    plt.plot(Rv,Rv*0,'xk',label='simulation points')
    plt.hist(samp,bins=20,alpha=.9, density=True,label='measurment distribution')
    plt.hist(Rv,bins=5,alpha=.5, density=True,label='simulation distribution')
    plt.xlabel('Mean lumen radius (um)')
    plt.ylabel('Probability density')
    plt.legend()
    figno+=1
    
    filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']=='LF')
    samp=dataPVS[filtre]['mean']
    param=sp.stats.lognorm.fit(samp, floc=0) # fit the sample data
    
    
    
    x=np.linspace(max(0,np.mean(samp)-3*np.std(samp)),np.mean(samp)+3*np.std(samp),100)
    pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=0, scale=param[2]) # fitted distribution
    h0=scipy.stats.lognorm.ppf(LHS[:,1], param[0], loc=0, scale=param[2])
    line2+=(' %.1e (%.1e) |'% (param[2],param[0]))
    
    h0mean=param[2]
   
    plt.figure(figno)
    plt.plot(x,pdf_fitted,'k-',label='fitted distribution')
    plt.plot(h0,h0*0,'xk',label='simulation points')
    plt.hist(samp,bins=20,alpha=.9, density=True,label='measurment distribution')
    plt.hist(h0,bins=5,alpha=.5, density=True,label='simulation distribution')
    plt.xlabel('Mean PVS thickness (um)')
    plt.ylabel('Probability density')
    plt.legend()
    figno+=1
    
    
    
    pdf_amp={}
    pdf_freq={}
    

    for bandname, name in zip(['cardiac','resp','LF','VLF'],['Cardiac','Respiratory','LF','VLF']):
        filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']==bandname)
        samp=dataPVS[filtre]['amp']
        param=sp.stats.lognorm.fit(samp,floc=0 ) # fit the sample data

        x=np.linspace(max(0,np.mean(samp)-3*np.std(samp)),np.mean(samp)+3*np.std(samp),100)
        pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=0, scale=param[2]) # fitted distribution
        amp=scipy.stats.lognorm.ppf(LHS[:,2], param[0], loc=0, scale=param[2])
        
        ampmean=param[2]
        
        plt.figure(figno)
        plt.plot(x,pdf_fitted,'k-',label='fitted distribution')
        plt.plot(amp,amp*0,'xk',label='simulation points')
        plt.hist(samp,bins=20,alpha=.9, density=True,label='measurment distribution')
        plt.hist(amp,bins=5,alpha=.5, density=True,label='simulation distribution')


        line3[bandname]+=(' %.1e (%.1e) |'% (param[2],param[0]))
        plt.xlabel(name+' oscillation amplitude (um)')
        plt.ylabel('Probability density')
        plt.legend()
        figno+=1
        

    
        filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']==bandname)
        samp=1/dataPVS[filtre]['period']
        param=sp.stats.lognorm.fit(samp, floc=0) # fit the sample data
        
        x=np.linspace(max(0,np.mean(samp)-3*np.std(samp)),np.mean(samp)+3*np.std(samp),100)
        pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=0, scale=param[2]) # fitted distribution
        freq=scipy.stats.lognorm.ppf(LHS[:,3], param[0], loc=0, scale=param[2])
        
        freqmean=param[2]
        
        plt.figure(figno)
        plt.plot(x,pdf_fitted,'k-',label='fitted distribution')
        plt.plot(freq,freq*0,'xk',label='simulation points')
        plt.hist(samp,bins=20,alpha=.9, density=True,label='measurment distribution')
        plt.hist(freq,bins=5,alpha=.5, density=True,label='simulation distribution')
        
        
        line4[bandname]+=(' %.1e (%.1e) |'% (param[2],param[0]))
        plt.xlabel(name+' oscillation frequency)')
        plt.ylabel('Probability density')
        plt.legend()
        figno+=1
        
        print('Generate simulation script '+stage)
        job=' -j '+mouse+'-'+study+'-'+stage+'-'+bandname+'_mean' 
        amplitudes=' -ai %e'%(ampmean/Rvmean)
        frequences=' -fi %e'%freqmean
        lumenradius=' -rv %e'%(Rvmean*1e-4)
        pvsradius= ' -rpvs %e'%(Rvmean*1e-4+h0mean*1e-4)
        
        f.write(simucall+job+default+amplitudes+frequences+lumenradius+pvsradius+diffusion+timeparams(freqmean,h0mean*1e-4,D)+'&\n')

        for i in range(N) :
     
            job=' -j '+mouse+'-'+study+'-'+stage+'-'+bandname+'_pt'+str(i) 
            amplitudes=' -ai %e'%(amp[i]/h0[i])
            frequences=' -fi %e'%freq[i]
            lumenradius=' -rv %e'%(Rv[i]*1e-4)
            pvsradius= ' -rpvs %e'%(Rv[i]*1e-4+h0[i]*1e-4)
 
            f.write(simucall+job+default+amplitudes+frequences+lumenradius+pvsradius+diffusion+timeparams(freq[i],h0[i]*1e-4,D)+'&\n')
        
        f.write('wait \n')
            
        
f.close()


        
markdown.append('## Mean lumen radius and PVS thickness')

markdown.append('| Variable |         Description | Distribution | REM median (std)  | NREM median (std) | IS median (std) |')

markdown.append('|----------|---------------------|--------------|----------------|-----------------| --------------|')



markdown.append(line1)
markdown.append(line2)

for bandname, name in zip(['cardiac','resp','LF','VLF'],['Cardiac','Respiratory','LF','VLF']):


    markdown.append('\n')
    markdown.append('## '+name+' oscillations')
    markdown.append('\n')
    
    markdown.append('| Variable |         Description | Distribution | REM median (std) | NREM median (std) | IS median (std) |')
    
    markdown.append('|----------|---------------------|--------------|----------------|-----------------| --------------|')
    
    markdown.append(line3[bandname])
    markdown.append(line4[bandname])

    markdown.append('\n')
    markdown.append('\n')

for l in markdown :
    print(l)
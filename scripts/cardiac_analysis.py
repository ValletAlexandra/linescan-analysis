#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 13:02:12 2022

@author: alexandra
"""
import numpy as np
from matplotlib import pyplot as plt 
plt.ioff()

import pandas as pd
import scipy.signal as sg
import os

import src.datanalysis as da

from matplotlib.patches import Rectangle

from os import listdir
from os.path import isfile, join

import regex as re

def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]

cardiac={'bandname':'cardiac','cutoff1':6,'cutoff2':9}


#select study name
name='PenetratingArteriolesWT4'
mousekind='WT'
mousenumber='10'
trial='04'
novessel='089'


# location of the database peak to peak analysis 
dirdatabase='/home/alexandra/Documents/Python/linescan-analysis/output/databases/'+name+'/'


### get the database with the peak to peak analysis

# set the data directory 
studylist=[]
#files=[]
for linescan in ['PVS1'] :
    f=dirdatabase+name+'_'+mousekind+'-'+mousenumber+'-'+trial+'-'+novessel+'-'+linescan+'.pkl'
    #files.append(f)
    dataf=pd.read_pickle(f)
    studylist.append(dataf)


    
    
    
#concanate

data=pd.concat([data for data in studylist], ignore_index=True)  

plt.figure()

for stage in ['Baseline','REM','NREM']:
    filter=(data['bandname']=='cardiac')&(data['stage']==stage)
    (data[filter]['amp']).hist(density=True, alpha=0.8, label=stage)
    
plt.legend()
plt.show()


plt.figure()

for stage in ['Baseline','REM','NREM']:
    filter=(data['bandname']=='cardiac')&(data['stage']==stage)
    plt.scatter(data[filter]['mean'],data[filter]['amp']/data[filter]['mean'], label=stage, alpha=0.5)

plt.title(linescan)
plt.ylabel('amplitude')
plt.xlabel('mean radius')    
plt.legend()
plt.show()

# look for the time of big oscillations

# take a time
tini=data[filter]['tini'].iloc[0]
tend=data[filter]['tend'].iloc[0]

file=data[filter]['file'].iloc[0]
#file='/home/alexandra/Documents/Data/Laura/Linescans/270122_4traces/PenetratingArterioles/WT 06 20201008 06 Penetrating Arteriole 044.csv'
#file='/home/alexandra/Documents/Data/Laura/Linescans/270122_4traces/PenetratingArterioles/WT 06 20201008 09 Penetrating Arteriole 045.csv'
datafile=pd.read_csv(file, decimal='.', delimiter=",")

time=datafile['t'].values

time_step=time[1]-time[0]
fs=1/time_step


I=np.where((time>tini)&(time<tend))
time=time[I]

signalraw={}
signalfiltered={}
mean={}

colors=['m','g','c','r','k']

for span_scanlines in [['lumen_upper', 'endfoot_upper'],['lumen_lower', 'endfoot_lower'],['lumen_lower', 'lumen_upper']] :
    for scanline in span_scanlines :
        
        signal=datafile[scanline].values/2
        
        #must keep only non nan values and interpolate for the missing values
        nans, indexes= nan_helper(signal)
        signal[nans]= np.interp(indexes(nans), indexes(~nans), signal[~nans])
        
        signalraw[scanline]=signal[I]
        signalfiltered[scanline]=da.bandpassfilter(signal[I]-np.mean(signal[I]), fs, cardiac['cutoff1'], cardiac['cutoff2'])
        mean[scanline]=np.mean(signal[I])
        
        
    plt.figure()
    for scanline,c in zip(span_scanlines,colors) :
        plt.plot(time,signalraw[scanline],c+':',label=scanline)
        plt.plot(time,signalfiltered[scanline]+mean[scanline],c+'-',label=scanline)
        plt.plot(time,signalfiltered[scanline],c+'--',label=scanline)
    
    tshift=(tend-tini)*0.4
        
    plt.xlim([tshift+tini,tshift+tini+2])
    plt.legend()
    plt.show()


plt.figure()


plt.plot(time,signalfiltered['lumen_lower'],label='lumen lower norm.')
plt.plot(time,signalfiltered['endfoot_lower']+mean['endfoot_lower']-mean['lumen_lower'], label='endfoot lower norm')

plt.plot(time,signalfiltered['endfoot_lower']-signalfiltered['lumen_lower'] +mean['endfoot_lower']-mean['lumen_lower'] , label='difference')


plt.xlim([tshift+tini,tshift+tini+2])
plt.legend()
plt.show()



plt.figure()


endfoot1=-(signalraw['endfoot_upper']-signalraw['endfoot_lower'])
endfoot1=da.lowpassfilter(endfoot1, fs, cardiac['cutoff2'])

endfoot2=-(signalfiltered['endfoot_upper']+mean['endfoot_upper']-signalfiltered['endfoot_lower']-mean['endfoot_lower'])


lumen1=-(signalraw['lumen_upper']-signalraw['lumen_lower'])
lumen1=da.lowpassfilter(lumen1, fs, cardiac['cutoff2'])

lumen2=-(signalfiltered['lumen_upper']+mean['lumen_upper']-signalfiltered['lumen_lower']-mean['lumen_lower'])

PVS1=(signalraw['lumen_upper']-signalraw['endfoot_upper'])
PVS1=da.lowpassfilter(PVS1, fs, cardiac['cutoff2'])

PVS2=-(signalraw['lumen_lower']-signalraw['endfoot_lower'])
PVS2=da.lowpassfilter(PVS2, fs, cardiac['cutoff2'])


PVS3=(endfoot1-lumen1)/2

PVS4=signalfiltered['lumen_upper']+mean['lumen_upper']-signalfiltered['endfoot_upper']-mean['endfoot_upper']

PVS5=-(signalfiltered['lumen_lower']+mean['lumen_lower']-signalfiltered['endfoot_lower']-mean['endfoot_lower'])


plt.plot(time,PVS1, label='lower PVS')
plt.plot(time,PVS2, label='upper PVS')
plt.plot(time,PVS3, label='diameter diff')
plt.plot(time,PVS4, label='filtered upper')
plt.plot(time,PVS5, label='filtered lower')


plt.plot(time,signalraw['endfoot_lower']-signalraw['lumen_lower'])
plt.plot(time,-signalraw['endfoot_upper']+signalraw['lumen_upper'])

plt.xlim([tshift+tini,tshift+tini+2])
plt.legend()
plt.show()

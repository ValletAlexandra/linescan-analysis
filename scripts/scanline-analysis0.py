#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 23:51:02 2021

@author: alexandra
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 14:21:29 2021

@author: alexandra
"""
import numpy as np
from matplotlib import pyplot as plt 
plt.ioff()

import pandas as pd
import scipy.signal as sg
import os

import src.datanalysis0 as da
import src.datanalysis as danew


from matplotlib.patches import Rectangle

from os import listdir
from os.path import isfile, join

import regex as re

def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]


# import data
study1={'study':'03','mouse':'WT08','file':'/home/alexandra/Documents/Data/Laura/Linescans/WT 08 20210309 03.csv'}
study2={'study':'06','mouse':'WT08','file':'/home/alexandra/Documents/Data/Laura/Linescans/WT 08 20210311 06.csv'}
study3={'study':'10','mouse':'WT08','file':'/home/alexandra/Documents/Data/Laura/Linescans/WT 08 20210311 10.csv'}
study4={'study':'03','mouse':'WT09','file':'/home/alexandra/Documents/Data/Laura/Linescans/WT 09 20210308 03.csv'}


studylist=[study1]#,study2,study3,study4


#to do : automatically construct the study dictionary with the file names.

# set the data directory 
dir='/home/alexandra/Documents/Data/Laura/Linescans/12102021/penetrating/WT/'

# get the list of files

files = [f for f in listdir(dir) if isfile(join(dir, f))]

studylist=[]

for f in files :

    split=re.split(r'\s|\.',f)
    mouse=split.pop(0)
    number=split.pop(0)
    date=split.pop(0)
    trial=split.pop(0)
    split.pop(-1)
    nofinal=split.pop(-1)
    vessel=' '.join(split)
    path=dir+f
    
    studylist.append({'trial':trial,'mouse':mouse, 'number':number, 'vessel':vessel, 'vesselID':nofinal, 'file':path}
)
    
#
# studyone={'trial':'16','mouse':'AQP4KO','number':'04','vessel':'Penetrating Arteriole', 'file':'/home/alexandra/Documents/Data/Laura/Linescans/30092021/AQP4KO 04 20201112 16 Penetrating Arteriole 012.csv'}
# studyone={'trial':'04','mouse':'AQP4KO','number':'03','vessel':'Penetrating Arteriole', 'file':'/home/alexandra/Documents/Data/Laura/Linescans/30092021/AQP4KO 04 20201112 16 Penetrating Arteriole 012.csv'}

# studylist=[studyone]




linescanlist=['lumen']#'PVS','lumen','endfoot'


cardiac={'bandname':'cardiac','cutoff1':4,'cutoff2':15}

resp={'bandname':'resp','cutoff1':1,'cutoff2':4}

lowfreq={'bandname':'LF','cutoff1':0.2,'cutoff2':1}

verylowfreq={'bandname':'VLF','cutoff1':0.05,'cutoff2':0.2}

continuous={'bandname':'continuous','cutoff1':0.0,'cutoff2':0.05}


bandfreqencies=[verylowfreq,lowfreq,resp,cardiac]

for study in studylist :
    plt.close('all')
    
    mousenumber=study['number']
    mousekind=study['mouse']
    trial=study['trial']
    vessel=study['vessel']
    file=study['file']
    nofinal=study['vesselID']
    
    print('Analysis of :',mousekind+' '+mousenumber+' '+trial+' '+vessel+' '+nofinal)
    
    
    #if os.path.isfile('../output/databases/amplitude_'+mousekind+'-'+mousenumber+'-'+trial+'-'+nofinal+'-'+'lumen'+'.pkl') :
    #    continue        
    data=pd.read_csv(file, decimal='.', delimiter=",")

    data['PVS']=data['endfoot']-data['lumen']

    # get sequences
    REM_list, NREM_list, IS_list, BASE_list, LOCO_list,WHISK_list,QUIET_list=danew.extract_sequences(data)

    # to do add the baseline output to the function
    
    for linescan in linescanlist :
        
        if np.isnan(data[linescan][0]) :
            continue

    
        # Analysis
        ampdata_list=[]
              
        
        for stage, seqlist in zip(['IS','NREM','REM','Baseline'],[IS_list,NREM_list,REM_list,BASE_list]):
            seqno=0
            for sequence in seqlist:
                
                outputdir='../output/amp-analysis/'+mousekind+'-'+mousenumber+'-'+trial+'-'+nofinal
                
                # Directory to store output data
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir)
               
                outputdir=outputdir+'/'
                
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir)
                    
                    
                #signal
                signal=data[linescan][sequence.ibegin:sequence.iend].values/2 ## We want radius
                
                #check for nan values
                nans, indexes= nan_helper(signal)
                
                #if there is more than 50% of missing data we skip
                if sum(nans)/len(signal)>=0.5 :
                    continue
                
                # remove the nan values at the begining or the end of the sequence
                ibegin=sequence.ibegin+np.where(~nans)[0][0]
                iend=sequence.ibegin+np.where(~nans)[0][-1]
                signal=data[linescan][ibegin:iend].values/2 ## We want radius
                
                #check for nan values
                nans, indexes= nan_helper(signal)
                #interpolate for the missing values
                signal[nans]= np.interp(indexes(nans), indexes(~nans), signal[~nans])


                time=data['t'][ibegin:iend].values-data['t'][ibegin]
                
                time_step=time[1]-time[0]
                fs=1/time_step
                
                #frequency,power=periodogram(data['lumen'].values,1/time_step,window=sg.hamming)
                frequency, Pxx_spec = sg.periodogram(signal,fs, 'hanning', scaling='density')
                
                
                fig, ax = plt.subplots()
                ax.plot(frequency,Pxx_spec)
                #plt.yscale('log')
                #plt.xlim([0,12])
                #plt.ylim([0,0.1])
        
                # Create a Rectangle patch
                rect = Rectangle((cardiac['cutoff1'],0 ),cardiac['cutoff2'] - cardiac['cutoff1'], 100, facecolor='r', alpha=0.4)
                # Add the patch to the Axes
                ax.add_patch(rect)


                # Create a Rectangle patch
                rect = Rectangle((resp['cutoff1'],0 ), resp['cutoff2'] - resp['cutoff1'], 100, facecolor='g', alpha=0.4)
                # Add the patch to the Axes
                ax.add_patch(rect)

                
                # Create a Rectangle patch
                rect = Rectangle((lowfreq['cutoff1'],0 ), lowfreq['cutoff2'] - lowfreq['cutoff1'], 100, facecolor='b', alpha=0.4)
                # Add the patch to the Axes
                ax.add_patch(rect)
                
                # Create a Rectangle patch
                rect = Rectangle((verylowfreq['cutoff1'],0 ), verylowfreq['cutoff2'] - verylowfreq['cutoff1'], 100, facecolor='m', alpha=0.4)
                # Add the patch to the Axes
                ax.add_patch(rect)
                
                # Create a Rectangle patch
                rect = Rectangle((continuous['cutoff1'],0 ), continuous['cutoff2'] - continuous['cutoff1'], 100, facecolor='k', alpha=0.4)
                # Add the patch to the Axes
                ax.add_patch(rect)
                
                plt.xscale('log')
                plt.yscale('log')
                
                #plt.xlim([0,12])
                #plt.ylim([0,100])
                
                plt.savefig(outputdir+'/spectrum_'+linescan+'-'+stage+str(seqno)+'.png')
                plt.close()
                
                #peak to peak analysis
                # in each frequency band

                fig, axs = plt.subplots(6,1,figsize=(16,9), gridspec_kw={'height_ratios': [1,1,1, 1,1,1]})
                axs[0].plot(time,signal)


                signal_continuous=da.bandpassfilter(signal-np.mean(signal), fs, continuous['cutoff1'], continuous['cutoff2'])
                
                axs[1].plot(time,signal_continuous)
                
                ifreq=0
                
                reconstruction=np.mean(signal)+signal_continuous
                
                for FB in bandfreqencies :

                #if sequence.end-sequence.begin > 2/cutoff2 :
                    
                    print(stage+'-'+str(seqno)+'-'+FB['bandname'])
                    
                    signal_filtered=da.bandpassfilter(signal-np.mean(signal), fs, FB['cutoff1'], FB['cutoff2'])
                    
                    reconstruction+=signal_filtered
                    
                    axs[ifreq+2].plot(time,signal_filtered)
                    ifreq+=1
                axs[0].plot(time,reconstruction)   
                
                for ax in axs[0:-1]:
                    ax.set_xticks([])
                
                plt.savefig(outputdir+'/decomposition_'+linescan+'-'+stage+str(seqno)+'.png')
                plt.close()
                
                for FB in bandfreqencies :
                    signal_filtered=da.bandpassfilter(signal-np.mean(signal), fs, FB['cutoff1'], FB['cutoff2'])

                    ampl,period,tvalley,tpeak,mean=danew.amp_analysis(signal,signal_filtered,time,FB['cutoff1'], FB['cutoff2'],export=linescan+'-'+stage+str(seqno)+'-'+FB['bandname'],outputdir=outputdir)
                    
                    if (np.array(mean)<0).any():
                        print('problem mean !!!')
                    
                    
                    #store data    
                    for a,p,m,t1,t2 in zip(ampl,period,mean,tvalley,tpeak) :
                        dictamp={'mouse kind':mousekind,'mouse number':mousenumber,'trial':trial, 'vessel':vessel, 'vesselID':nofinal,'linescan':linescan,'cutoff1':FB['cutoff1'],'cutoff2':FB['cutoff2'],'sequence':seqno,'stage':stage,'tini':sequence.begin,'tend':sequence.end, 'bandname':FB['bandname']}
                        dictamp['amp']=a
                        dictamp['period']=p
                        dictamp['mean']=m
                        dictamp['tmin']=t1+data['t'][ibegin]
                        dictamp['tmax']=t2+data['t'][ibegin]
                        ampdata_list.append(dictamp)
                        
                    
                seqno+=1
        
        amplitudedb = pd.DataFrame(ampdata_list)
        
        
        ### For normalisation we compute meanvalues during the baseline stage
        
        amplitudedb['mean base radius']=np.nan
        amplitudedb['mean base period']=np.nan
        

        # The mean radius is taken over the time scales of the LF oscillations
        filter=(amplitudedb['stage']=='Baseline')&(amplitudedb['bandname']=='LF')
        meanradius=np.median(amplitudedb['mean'][filter])
        amplitudedb['mean base radius']=meanradius
           
        # for each frequency band we compute the mean period during the baseline stage
        for FB in bandfreqencies :
            bandname=FB['bandname']
            filter=(amplitudedb['stage']=='Baseline')&(amplitudedb['bandname']==bandname)
            meanperiod=np.median(amplitudedb['period'][filter])
            amplitudedb['mean base period'][(amplitudedb['bandname']==bandname)]=meanperiod

        # Normalised values compared to the baseline stage    
        amplitudedb['amp norm']=amplitudedb['amp']/amplitudedb['mean base radius']
        amplitudedb['mean norm']=amplitudedb['mean']/amplitudedb['mean base radius']
        amplitudedb['period norm']=amplitudedb['period']/amplitudedb['mean base period']
            
            
          
        
        print('save:../output/databases/amplitude_'+mousekind+'-'+mousenumber+'-'+trial+'-'+nofinal+'-'+linescan+'.pkl')
        amplitudedb.to_pickle('../output/databases/oldanalysislist/amplitude_'+mousekind+'-'+mousenumber+'-'+trial+'-'+nofinal+'-'+linescan+'.pkl')


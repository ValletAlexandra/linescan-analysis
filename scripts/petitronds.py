#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 15:49:50 2021

@author: alexandra
"""

## test jolis petits ronds

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
from scipy import signal

fig = plt.figure()
ax = fig.add_subplot(projection='3d')


# let say we have a distribution of enhancement factors :
Enhancement=scipy.stats.lognorm(0.5,loc=0,scale=1).rvs(size=2000) # sample
    
# we fit a pdf

param=scipy.stats.lognorm.fit(Enhancement,loc=0 ) # fit the sample data

#x=np.linspace(max(0,np.mean(Enhancement)-1*np.std(Enhancement)),np.mean(Enhancement)+1*np.std(Enhancement),100)
r=np.linspace(0.1,5,50)
pdf_fitted = scipy.stats.lognorm.pdf(r, param[0], loc=param[1], scale=param[2])# fitted distribution

pdf_fitted=pdf_fitted/pdf_fitted.max()

# Create the mesh in polar coordinates and compute corresponding Z.

p = np.linspace(0, 2*np.pi, 50)
R, P = np.meshgrid(r, p)
Z =scipy.stats.lognorm.pdf(R, param[0], loc=param[1], scale=param[2])
Z=Z/Z.max()

# Express the mesh in the cartesian system.
X, Y = R*np.cos(P), R*np.sin(P)

# Plot the surface.
ax.plot_surface(X, Y, Z, cmap=plt.cm.BuPu)

# Tweak the limits and add latex math labels.
ax.set_zlim(0, 1)
ax.set_xlabel(r'$\phi_\mathrm{real}$')
ax.set_ylabel(r'$\phi_\mathrm{im}$')
ax.set_zlabel(r'$V(\phi)$')

fig=plt.figure()

ax = fig.add_subplot(111,aspect='equal')

for ri in range(1,5) :
    x , y = ri*np.cos(p), ri*np.sin(p)
    plt.plot(x,y,'grey', linewidth=0.5)
    
ax.contourf(X, Y, Z,50, zdir='z', offset=-1.5,cmap=plt.cm.Blues)
ax.plot(-0.02,0,'.k')
plt.setp(ax.spines.values(), visible=False)
# remove ticks and labels 
ax.tick_params(left=False, labelleft=False)
ax.tick_params(bottom=False, labelbottom=False)

plt.show()

#8     disp-d1e-07-l2e-02-stageREM-VLF-v8  0.048765
#103    disp-d1e-07-l2e-02-stageIS-VLF-v8  0.160764
#199  disp-d1e-07-l2e-02-stageNREM-VLF-v8  0.314380
#295   disp-d1e-07-l2e-02-baseline-VLF-v8  0.059479

#55    disp-d1e-07-l2e-02-stageREM-LF-v8  0.133113
#151    disp-d1e-07-l2e-02-stageIS-LF-v8  0.571799
#247  disp-d1e-07-l2e-02-stageNREM-LF-v8  0.850330
#343   disp-d1e-07-l2e-02-baseline-LF-v8  0.197968

E={}
E['baseline']={}
E['baseline']['VLF']=0.059479
E['baseline']['LF']=0.197968
E['baseline']['Total']=E['baseline']['VLF']+E['baseline']['LF']

E['NREM']={}
E['NREM']['VLF']=0.314380
E['NREM']['LF']=0.850330
E['NREM']['Total']=E['NREM']['VLF']+E['NREM']['LF']

E['REM']={}
E['REM']['VLF']=0.048765
E['REM']['LF']=0.133113
E['REM']['Total']=E['REM']['VLF']+E['REM']['LF']

fig2=plt.figure()
gs=fig2.add_gridspec(3, 3, hspace=0, wspace=0)
axs2=gs.subplots(sharex='col', sharey='row')

theta = np.linspace(0, 2*np.pi, 100)
emax=5

for i, stage in enumerate(['baseline','NREM','REM']) :
    for j, bandname in enumerate(['VLF','LF','Total']) :
        Z =scipy.stats.uniform(0, 4*E[stage][bandname]).pdf(R)
        Z=Z/Z.max()
        
        for ri in range(1,5) :
            x , y = ri*np.cos(p), ri*np.sin(p)
            axs2[i,j].plot(x,y,'grey', linewidth=0.5)
        
            
        axs2[i,j].contourf(X, Y, Z,50, zdir='z', offset=-1.5,cmap=plt.cm.Blues)
        #axs2[i,j].plot(-0.02,0,'.k')
        plt.setp(ax.spines.values(), visible=False)
        # remove ticks and labels 
        axs2[i,j].tick_params(left=False, labelleft=False)
        axs2[i,j].tick_params(bottom=False, labelbottom=False)
        
        axs2[i,j].set_aspect(1)
        axs2[i,j].set_xlim([-emax,emax])
        axs2[i,j].set_ylim([-emax,emax])
                
        axs2[i,j].set_xlabel(bandname)
        axs2[i,j].set_ylabel(stage)
        
        axs2[i,j].text(-1,-0.5, '%i'%(E[stage][bandname]*100))
        
        
for ax in axs2.flat:
    ax.label_outer()
    
    
plt.savefig('/home/alexandra/Documents/Articles/Sleep/Images/enhancement.svg')
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 15:28:49 2022

@author: alexandra
"""

import pickle
import pandas as pd
from matplotlib import pyplot as plt 
from scipy.stats import mannwhitneyu, normaltest
import seaborn as sbn
from statannot import add_stat_annotation
import numpy as np

import os
from os import listdir
from os.path import isfile, join

from scipy import stats

import itertools

dir='../output/databases/PenetratingArteriolesWT6/'

# get the list of files

files = [f for f in listdir(dir) if isfile(join(dir, f))]

studylist=[]

for f in files : 
    data=pd.read_pickle(dir+f)
    studylist.append(data)
    
amplitudedb=pd.concat([data for data in studylist], ignore_index=True)




frequencyband='VLF'
linescan='PVS'

states='all'
states='awake'
states='sleep'
#states='NREM'

filter=amplitudedb['linescan']==linescan
filter=filter&(amplitudedb['bandname']==frequencyband)

if states=='sleep' :
    filter=filter&((amplitudedb['stage']=='baseline')|(amplitudedb['stage']=='IS')|(amplitudedb['stage']=='NREM')|(amplitudedb['stage']=='REM'))
elif states=='awake' :
    filter=filter&~((amplitudedb['stage']=='baseline')|(amplitudedb['stage']=='IS')|(amplitudedb['stage']=='NREM')|(amplitudedb['stage']=='REM'))
elif states=='NREM' :
    filter=filter&((amplitudedb['stage']=='NREM'))


filter=filter&(amplitudedb['hasnan']==False)
if frequencyband=='cardiac':
    filter=filter&(amplitudedb['outlier']==False)
#filter=filter&(amplitudedb['meanepisode']<12)

#filter=filter&(amplitudedb['corrlumen']>0.90)
#filter=filter&(amplitudedb['correndfoot']>0.80)

# filter rigid motion 
rigidmotion= (amplitudedb['bandname']=='VLF')*1.5 + (amplitudedb['bandname']=='LF')*0.8 + (amplitudedb['bandname']=='resp')*0.4 + (amplitudedb['bandname']=='cardiac')*0.5
theta=np.arcsin(rigidmotion/amplitudedb['meanlumen'])
rigidartefact=amplitudedb['meanlumen']*(1-np.cos(theta))
filter=filter&(amplitudedb['amp']>rigidartefact)

amplitudedb['amp corrected']=amplitudedb['amp']-rigidartefact

#filter=filter&((amplitudedb[filter]['amp']/amplitudedb[filter]['meanepisode'])>0.15)

#filter=filter&(amplitudedb[filter]['stage']=='Baseline')&(amplitudedb[filter]['mouse number']!='06')&(amplitudedb[filter]['mouse number']!='09')

print(amplitudedb[filter][['mouse number','stage','trial','vesselID','tmin','amp','corrlumen']])

#amplitudedb[filter]['corrr'].hist()

(amplitudedb[filter]['amp']/amplitudedb[filter]['meanepisode']).hist()

#plt.scatter(amplitudedb[filter]['mean'],amplitudedb[filter]['amp']/amplitudedb[filter]['mean'])
plt.figure()

plt.scatter(amplitudedb[filter]['meanlumen'],amplitudedb[filter]['amp']/amplitudedb[filter]['meanepisode'])


import seaborn as sns

amplitudedb['vessel identification']=amplitudedb['mouse number']+amplitudedb['trial']+amplitudedb['vesselID']
dataplot=amplitudedb[filter]

sns.lmplot('meanlumen', 'amp', data=dataplot, hue='vessel identification', fit_reg=False)

plt.ylabel(frequencyband+' mean (um) '+linescan)
plt.title(states+' states')
plt.show()




frequencyband='cardiac'

filter=(amplitudedb['bandname']==frequencyband)
if states=='sleep' :
    filter=filter&((amplitudedb['stage']=='baseline')|(amplitudedb['stage']=='IS')|(amplitudedb['stage']=='NREM')|(amplitudedb['stage']=='REM'))
elif states=='awake' :
    filter=filter&~((amplitudedb['stage']=='baseline')|(amplitudedb['stage']=='IS')|(amplitudedb['stage']=='NREM')|(amplitudedb['stage']=='REM'))
elif states=='NREM' :
    filter=filter&((amplitudedb['stage']=='NREM'))
filter=filter&(amplitudedb['hasnan']==False)
if frequencyband=='cardiac':
    filter=filter&(amplitudedb['outlier']==False)


vesselIDs=np.unique(amplitudedb[filter]['vessel identification'])

filterlumen=filter&(amplitudedb['linescan']=='rigid lumen')
filterlumen=filterlumen&(amplitudedb['corrlumen']>0.90)

filterendfoot=filter&(amplitudedb['linescan']=='rigid endfoot')
filterendfoot=filterendfoot&(amplitudedb['correndfoot']>0.90)

filterPVS=filter&(amplitudedb['linescan']=='PVS')
filterPVS=filterPVS&(amplitudedb['correndfoot']>0.70)
filterPVS=filterPVS&(amplitudedb['corrlumen']>0.70)

for vesselid in vesselIDs :
    plt.figure()
    filtervessel=amplitudedb['vessel identification']==vesselid
    plt.hist(amplitudedb[filterendfoot&filtervessel]['amp'],label='rigid endfoot',alpha=0.5,density=True)
    plt.hist(amplitudedb[filterlumen&filtervessel]['amp'],label='rigid lumen',alpha=0.5,density=True)
    plt.title('vessel '+str(vesselid))
    plt.legend()

for vesselid in vesselIDs :
    plt.figure()
    filtervessel=amplitudedb['vessel identification']==vesselid
    plt.hist(amplitudedb[filterPVS&filtervessel]['meanepisode'],alpha=0.5,density=True)
    plt.title('vessel '+str(vesselid))

    
    print('vessel :'+str(vesselid))
    print('mean PVS :',np.mean(amplitudedb[filterPVS&filtervessel]['meanepisode']))

    
amplitudedb['time']=amplitudedb['tini'].astype('str')
amplitudedb['vessel time']=amplitudedb['vessel identification']+amplitudedb['time']

frequencyband='cardiac'
filter=amplitudedb['linescan']=='lumen'
filter=filter&(amplitudedb['bandname']==frequencyband)
filter=filter&(amplitudedb['corrlumen']>0.80)

filtersleep=filter&((amplitudedb['stage']=='baseline')|(amplitudedb['stage']=='IS')|(amplitudedb['stage']=='NREM')|(amplitudedb['stage']=='REM'))
filterawake=filter&~((amplitudedb['stage']=='baseline')|(amplitudedb['stage']=='IS')|(amplitudedb['stage']=='NREM')|(amplitudedb['stage']=='REM'))

plt.figure()
plt.hist(np.unique(amplitudedb[filtersleep]['meanepisode']),alpha=0.5,label='sleep')
plt.hist(np.unique(amplitudedb[filterawake]['meanepisode']),alpha=0.5,label='awake')
plt.legend()

plt.xlabel('radius (um)')

filter=filtersleep&(amplitudedb['meanepisode']<4)
np.unique(amplitudedb[filter]['vessel time'])
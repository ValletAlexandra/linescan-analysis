#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 13:08:56 2022

@author: alexandra
"""

import pickle
import pandas as pd
from matplotlib import pyplot as plt 
from scipy.stats import mannwhitneyu, normaltest
import seaborn as sbn
from statannot import add_stat_annotation
import numpy as np

import os
from os import listdir
from os.path import isfile, join

from scipy import stats

import itertools

colors=['darkorchid','mediumseagreen','darkorange','blue']
my_pal = {"REM": "darkorchid", 
          "NREM": "seagreen", 
          "IS":"darkorange", 
          "Baseline":"royalblue",
          "Awakening":"tomato",
          "REM WT": "darkorchid", 
          "NREM WT": "seagreen", 
          "IS WT":"darkorange", 
          "Baseline WT":"royalblue",
          "REM AQP4KO": "mediumorchid", 
          "NREM AQP4KO": "mediumseagreen", 
          "IS AQP4KO":"moccasin", 
          "Baseline AQP4KO":"cornflowerblue",
          "Quiet":"dodgerblue",
          "Locomotion":"tomato",
          "Whisking":"lightsalmon"}

legends={'period':'Oscillation period (s)','amp':'Oscillation amplitude (um)','mean':'Radius mean value','amp Area pc':'Oscillation amplitude of the Area (pc)','amp pc':'Oscillation amplitude (pc)','amp norm':'Normalized amplitude','period norm':'Normalized period','mean norm':'Normalized mean radius',
         'amp area from thickness pc':'Oscillation amplitude of the Area (pc) deduced from the thickness'}
    

dir='../output/databases/PenetratingArteriolesWT8/averaged/'

outputdir='../output/statistics/PenetratingArteriolesWT8/'
# create a folder for the slurm files and the batch file
if not os.path.exists(outputdir):
        os.makedirs(outputdir)
        
# Is NREM VLF different if we have contact or not ? 
# Is amp endfoot kinf of sudently higher for a given size of PVS? or amp of the lumen /PVS ratio ?
# Is the amp of endfoot same in both direction for cross linescan ?

# get the list of files




files = [f for f in listdir(dir) if isfile(join(dir, f))]

studylist=[]

for f in files : 
    data=pd.read_pickle(dir+f)
    studylist.append(data)
    
averagedb=pd.concat([data for data in studylist], ignore_index=True)


sleepstages=[('Baseline', 'NREM'),
              ('Baseline', 'IS'),
              ('Baseline', 'REM'),
              ('Awakening','Baseline')]

combinations={'stage':sleepstages}


### Comparison between states plots
hist=False
violin=True

# chose the variables
bandnamelist=['cardiac','resp','LF','VLF']

variablelist=['amp Area pc','amp area from thickness pc'] ### ratio of amplitude oscillation depending on state

stages=['Baseline', 'NREM', 'IS', 'REM', 'Awakening']

from itertools import combinations_with_replacement

combinations=list(combinations_with_replacement(stages, 2))
combis=[combi for combi in combinations if (combi[0] in stages)&(combi[1] in stages)]



### create new variables
for bandname in bandnamelist:
    averagedb['amp Area pc'+' '+bandname]=averagedb['amp Area '+bandname]/averagedb['mean Area']/2*100 # divided by two because amp Area correspond to the peak to peak


    # compute the area change from the thickness change
    rpvs=averagedb['mean endfoot']/2*1e-4 #cm
    rv=averagedb['mean lumen']/2*1e-4 #cm
    
    amp_pvs=averagedb['amp PVS '+bandname]/2*1e-4 #cm
    amp_endfoot=averagedb['amp endfoot '+bandname]/2 *1e-4 #cm
    amp_lumen=averagedb['amp lumen '+bandname]/2 *1e-4 #cm
                        
    A0=np.pi*(rpvs)**2- np.pi*(rv)**2
        
    # So what we do is to fix Rpvs and compute area change using the change of thickness
    Amin=np.pi*(rpvs)**2- np.pi*(rv+amp_pvs)**2
    Amax=np.pi*(rpvs)**2- np.pi*(rv-amp_pvs)**2        
                        
    ai=(Amax-Amin)/A0
                        
    ai/=2
    
    averagedb['amp area from thickness pc'+' '+bandname]=ai


for bandname in bandnamelist:
        
    
        for variable in variablelist:
            
            if hist :
       
                plt.figure() 
                for stage,c in zip(stages,colors):
                    filter=(averagedb['stage']==stage)
                    
                    dataplot=averagedb[filter]
                    
                    sbn.histplot(data=dataplot,x=variable+' '+bandname,palette="crest",color=c,label=stage,stat="density")
                    
                    print('median '+variable+' '+bandname+' '+stage+':',dataplot[variable+' '+bandname].median())
                    
                    plt.savefig(outputdir+'/'+legends[variable]+' '+bandname+'_hist.png')
                    
            filter=(averagedb['stage'].isin(stages))
            dataplot=averagedb[filter]
            #dataplot.replace([np.inf, -np.inf], np.nan, inplace=True)
            dataplot.dropna(axis=0,how='any')
                    
            if violin :
                
                plt.figure(figsize=(10, 5)) 
                
                
                yposlist = (dataplot.groupby(['stage'])[variable+' '+bandname].median()[stages]).tolist()
                Nlist = (dataplot.groupby(['stage'])[variable+' '+bandname].size()[stages]).tolist()                
                maxlist = (dataplot.groupby(['stage'])[variable+' '+bandname].max()[stages]).tolist()                

                xposlist = np.array(range(len(yposlist)))
                stringlist = ['%.2f, N=%i'%(m,s) for m,s in zip(yposlist,Nlist)]
                
                
                ax=sbn.violinplot(x='stage', y=variable+' '+bandname, data=dataplot, order=stages,alpha=0.3, palette=my_pal, cut=0)  
               # sbn.swarmplot(data=dataplot,x="stage", y=variable, color="white", edgecolor="gray")
                ax.set_xticklabels(ax.get_xticklabels(),rotation = 30)
                plt.ylabel(legends[variable])
                plt.title(legends[variable]+' '+bandname)
                
                for i in range(len(stringlist)):
                    ax.text(xposlist[i]-0.3, maxlist[i]*1.02, stringlist[i])
    
               # combinations=list(itertools.combinations(list(np.unique(dataplot[comparison_variable])), 2))
                
                add_stat_annotation(ax, data=dataplot, x='stage', y=variable+' '+bandname, order=stages,box_pairs=combis,
                                    test='Mann-Whitney',  comparisons_correction=None, text_format='star',verbose=2)
                
                plt.tight_layout()                

                plt.savefig(outputdir+'/'+legends[variable]+' '+bandname+'_violin.png')





### Scatter plot to look for interesting relationship in the data

averagedb['vessel identification']=averagedb['mouse number']+averagedb['trial']+averagedb['vesselID']
averagedb['ratio cardiac']=averagedb['amp endfoot cardiac']/averagedb['amp lumen cardiac']
averagedb['ratio VLF']=averagedb['amp endfoot VLF']/averagedb['amp lumen VLF']

averagedb['ratio rigid cardiac']=averagedb['amp rigid endfoot cardiac']/averagedb['amp rigid lumen cardiac']

filter=averagedb['corrlumen cardiac']>0.5
filter=filter&(averagedb['correndfoot cardiac']>0.5)

dataplot=averagedb[filter]

x='amp endfoot VLF'
y='amp PVS VLF'

sbn.lmplot(x, y, data=dataplot, hue='vessel identification', fit_reg=False)

#plt.ylabel(' amp PVS VLF ')
#plt.xlabel(' ratio amp cardiac endfoot/lumen ')

plt.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 15:12:14 2021

@author: alexandra
"""
import numpy as np

#variable='tortuosity'
#spanth=np.linspace(0.1, 1.5,20) #tortuosity

#variable='rawvar'
#spanth=np.linspace(0.01, 0.4,20) #rawvar

#variable='HFerror'
#spanth=np.linspace(0.01, 0.2,20) #rawvar

#variable='HFerrormax'
#spanth=np.linspace(0.01, 0.6,20) #rawvar

#variable='ampvar'
#spanth=np.linspace(0.01, 0.06,20) #rawvar

#variable='periodvar'
#spanth=np.linspace(0.01, 0.1,20) #rawvar

variable='slopevar'
spanth=np.linspace(0.01, 4,20) #rawvar

#0.01,0.02,0.03,0.04,
ampmax=[]
ampmean=[]

amplitudedb=pd.concat([data for data in studylist], ignore_index=True)
    
# add new variables
amplitudedb['amp pc']=amplitudedb['amp']/amplitudedb['mean']*100

for th in spanth :

    filter=(amplitudedb['linescan']==scanline)
    
    # remove the cardiac pulsation when too much noise
    filter=filter& np.logical_not((amplitudedb[variable]>th))
   
    filter=filter&(amplitudedb['bandname']=='cardiac')

    if sum(filter) ==0:
        break
    

    
    ampmean.append(amplitudedb[filter]['amp pc'].median())
    ampmax.append(amplitudedb[filter]['amp pc'].max())

plt.figure()
plt.plot(np.array(spanth),ampmax,'o', label='max')
plt.xlabel('Error treashold (um)')
plt.ylabel('Amplitude (pc)' )
plt.show()

plt.figure()
plt.plot(np.array(spanth),ampmean,'o',label='mean')
plt.xlabel('Error treashold (um)')
plt.ylabel('Amplitude (pc)' )
plt.show()
#plt.legend()
#plt.xscale('log')
#plt.yscale('log')

plt.show()
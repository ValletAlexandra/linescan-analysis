#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 20:17:23 2022

@author: alexandra
"""
# Post processing of the PVSBrain simulation in order to get :
# the maximal velocity and equivalent analytical change of area, 
# the change of area and volume of the PVS
# the flow in SAS and the flow in 


### Data dict from the WT6 filter analysis : 
expData={'baseline': {'card': {'Rv': 0.0004374610081159935,
   'h0': 0.00036022576500372984,
   'ai': 0.014140834165106472,
   'fi': 8.262675952588584,
   'Umax': 0.014682689476770417},
  'resp': {'Rv': 0.0004374610081159935,
   'h0': 0.00036022576500372984,
   'ai': 0.010548976726131969,
   'fi': 2.5398430379712953,
   'Umax': 0.003366875646472067},
  'LF': {'Rv': 0.0004374610081159935,
   'h0': 0.00036022576500372984,
   'ai': 0.011049849553261645,
   'fi': 0.5198917338071122,
   'Umax': 0.0007219034898885877},
  'VLF': {'Rv': 0.0004374610081159935,
   'h0': 0.00036022576500372984,
   'ai': 0.020623883160841724,
   'fi': 0.20299832938372017,
   'Umax': 0.0005261054096998516}},
 'stageNREM': {'card': {'Rv': 0.0004374610081159935,
   'h0': 0.00036022576500372984,
   'ai': 0.014140834165106472,
   'fi': 7.799804249274825,
   'Umax': 0.013860171260355238},
  'resp': {'Rv': 0.0004374610081159935,
   'h0': 0.00036022576500372984,
   'ai': 0.006147383884007037,
   'fi': 2.5398430379712953,
   'Umax': 0.001962036472912703},
  'LF': {'Rv': 0.0004374610081159935,
   'h0': 0.00036022576500372984,
   'ai': 0.026030286456027046,
   'fi': 0.5198917338071122,
   'Umax': 0.0017005982339242632},
  'VLF': {'Rv': 0.0004374610081159935,
   'h0': 0.00036022576500372984,
   'ai': 0.020623883160841724,
   'fi': 0.20299832938372017,
   'Umax': 0.0005261054096998516}},
 'stageIS': {'card': {'Rv': 0.0005107343629851353,
   'h0': 0.00032674474781703645,
   'ai': 0.010945555686526429,
   'fi': 7.796331513659636,
   'Umax': 0.010723535075390062},
  'resp': {'Rv': 0.0005107343629851353,
   'h0': 0.00032674474781703645,
   'ai': 0.011629913675935443,
   'fi': 2.5398430379712953,
   'Umax': 0.003711874065385024},
  'LF': {'Rv': 0.0005107343629851353,
   'h0': 0.00032674474781703645,
   'ai': 0.023271983149213984,
   'fi': 0.5198917338071122,
   'Umax': 0.0015203940805770513},
  'VLF': {'Rv': 0.0005107343629851353,
   'h0': 0.00032674474781703645,
   'ai': 0.02273717982797332,
   'fi': 0.20299832938372017,
   'Umax': 0.0005800145983918042}},
 'stageREM': {'card': {'Rv': 0.0006990167377251289,
   'h0': 0.0002692249030307004,
   'ai': 0.018920585531181297,
   'fi': 10.022619523972866,
   'Umax': 0.023830089881697544},
  'resp': {'Rv': 0.0006990167377251289,
   'h0': 0.0002692249030307004,
   'ai': 0.014114642324689052,
   'fi': 2.5398430379712953,
   'Umax': 0.004504915190867485},
  'LF': {'Rv': 0.0006990167377251289,
   'h0': 0.0002692249030307004,
   'ai': 0.009250237018809703,
   'fi': 0.5198917338071122,
   'Umax': 0.0006043320638880747},
  'VLF': {'Rv': 0.0006990167377251289,
   'h0': 0.0002692249030307004,
   'ai': 0.027594973590220125,
   'fi': 0.20299832938372017,
   'Umax': 0.0007039345972393894}}}

import pickle
import pandas as pd
from matplotlib import pyplot as plt 
from scipy.stats import mannwhitneyu, normaltest
import seaborn as sbn
from statannot import add_stat_annotation
import numpy as np

import os
from os import listdir
from os.path import isfile, join

from scipy import stats

import itertools

# Maximal velocity and analytical change of area associated
stage='stageREM'
frequencyband='card'
K='1e-14'
E='2'
hmem='2'


stagelist=['baseline','stageNREM','stageIS','stageREM']
FB=['card','resp','LF','VLF']
spanK=['1e-14','1e-15']
spanE=['2','5','10']
spanhmem=['1','2']

for stage in stagelist :
    for frequencyband in FB :
        plt.figure()
        expDataUmax=expData[stage][frequencyband]['Umax']
        print('Umax from data:',expDataUmax*1e4)
    
        plt.plot([0,50],[expDataUmax*1e4,expDataUmax*1e4],'k:')
        plt.plot([0,50],[-expDataUmax*1e4,-expDataUmax*1e4],'k:')
        plt.xlabel('time (s)')
        plt.ylabel('velocity (um/s)')
        plt.title('Max velocity,'+stage+', '+frequencyband)
        
        
        for E in spanE :
            # load the data
            data=np.loadtxt('/home/alexandra/Data/SleepOutput/deformationWT6/PVSBrain-l2e-02-hmem'+hmem+'e-04-E'+E+'-K'+K+'-'+stage+'-'+frequencyband+'_velocity.txt',delimiter=',')
            time=data[1:,0]
            maxvelocity=data[1:,2]
            print('hmem'+hmem+'e-04-E'+E+'-K'+K)
            print('Simulated Umax:',max(maxvelocity)*1e4)
            plt.plot(time,maxvelocity*1e4, label='K='+K+'cm2, E='+E+'kPa'+', hmem='+hmem+'um,'+stage)  
            plt.xlim([time[0],time[-1]])
    
        plt.legend()


# Radius of the astrocyte endfeet
for stage in stagelist :
    for frequencyband in FB :
        plt.figure()
    
        plt.xlabel('time (s)')
        plt.ylabel('astrocyte radius (um)')
        plt.title('Astrocyte endfeet,'+stage+', '+frequencyband)
        
        
        for E in spanE :
            # load the data
            data=np.loadtxt('/home/alexandra/Data/SleepOutput/deformationWT6/PVSBrain-l2e-02-hmem'+hmem+'e-04-E'+E+'-K'+K+'-'+stage+'-'+frequencyband+'_fluiddomain.txt',delimiter=',')
            time=data[1:,0]
            y=data[1:,3]

            plt.plot(time,y*1e4, label='K='+K+'cm2, E='+E+'MPa'+', hmem='+hmem+'um,'+stage)  
            plt.xlim([time[0],time[-1]])
    
        plt.legend()
      
plt.show()

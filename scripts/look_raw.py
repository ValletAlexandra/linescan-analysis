#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  1 16:09:36 2021

@author: alexandra
"""

import pickle
import pandas as pd
from matplotlib import pyplot as plt 
from scipy.stats import mannwhitneyu, normaltest
import seaborn as sbn
from statannot import add_stat_annotation
import numpy as np
import src.datanalysis as da
from os import listdir
from os.path import isfile, join

from scipy import stats

import itertools

def arc_length(x, y):
    npts = len(x)
    arc = np.sqrt((x[1] - x[0])**2 + (y[1] - y[0])**2)
    for k in range(1, npts):
        arc = arc + np.sqrt((x[k] - x[k-1])**2 + (y[k] - y[k-1])**2)

    return arc


def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]

#amplitudedb[amplitudedb['amp pc']>50][['mouse number','trial','bandname','tmin','amp pc']]


cardiac={'bandname':'cardiac','cutoff1':4,'cutoff2':15}

resp={'bandname':'resp','cutoff1':1,'cutoff2':4}

lowfreq={'bandname':'LF','cutoff1':0.2,'cutoff2':1}

verylowfreq={'bandname':'VLF','cutoff1':0.05,'cutoff2':0.2}

continuous={'bandname':'continuous','cutoff1':0.0,'cutoff2':0.05}




FB=cardiac
study={'vesselID':'058','trial':'11','mouse':'WT','number':'06','vessel':'Penetrating Arteriole'}
study['file']='/home/alexandra/Documents/Data/Laura/Linescans/12102021/AQP4KO 06 20201214 10 Penetrating Arteriole 021.csv'
#study['file']='/home/alexandra/Documents/Data/Laura/Linescans/12102021/AQP4KO 03 20210111 04 Penetrating Arteriole 143.csv'
#study['file']='/home/alexandra/Documents/Data/Laura/Linescans/12102021/AQP4KO 01 20201001 08 Penetrating Arteriole 002.csv'
#study['file']='/home/alexandra/Documents/Data/Laura/Linescans/12102021/WT 10 20210422 10 Penetrating Arteriole 109.csv'
#study['file']='/home/alexandra/Documents/Data/Laura/Linescans/12102021/AQP4KO 04 20201105 11 Penetrating Arteriole 008.csv'

#study['file']='/home/alexandra/Documents/Data/Laura/Linescans/12102021/WT 06 20200929 08 Penetrating Arteriole 000.csv'
#'/home/alexandra/Documents/Data/Laura/Linescans/12102021/WT 06 20200929 07 Penetrating Arteriole 000.csv'
#'/home/alexandra/Documents/Data/Laura/Linescans/12102021/WT 06 20200929 05 Penetrating Arteriole 000.csv'
#'/home/alexandra/Documents/Data/Laura/Linescans/12102021/WT 04 20200915 10 Penetrating Arteriole 000.csv'
#'/home/alexandra/Documents/Data/Laura/Linescans/12102021/WT 02 20200909 13 Penetrating Arteriole 000.csv'
tmin=29
tend=32 #tmin+5/FB['cutoff1']


mousenumber=study['number']
mousekind=study['mouse']
trial=study['trial']
vessel=study['vessel']
file=study['file']
nofinal=study['vesselID']



#amplitudedb=pd.read_pickle('../output/databases/amplitude_'+mousekind+'-'+mousenumber+'-'+trial+'-'+nofinal+'-'+scanline+'.pkl')
#amplitudedb['amp pc']=amplitudedb['amp']/amplitudedb['mean']*100
#print(amplitudedb[amplitudedb['amp pc']>40][['tmin','amp pc','bandname']])



data=pd.read_csv(file, decimal='.', delimiter=",")
data['PVS']=data['endfoot']-data['lumen']


time=data['t'].values

# scanline='endfoot'
# signal=data[scanline].values/2

# #must keep only non nan values and interpolate for the missing values
# nans, indexes= nan_helper(signal)
# signal[nans]= np.interp(indexes(nans), indexes(~nans), signal[~nans])

time_step=time[1]-time[0]
fs=1/time_step


I=np.where((time>tmin)&(time<tend))
time=time[I]
# signalendfoot=signal[I]
# signalendfoot_filtered=da.bandpassfilter(signal-np.mean(signal), fs, FB['cutoff1'], FB['cutoff2'])[I]


scanline='lumen'
signal=data[scanline].values/2

#must keep only non nan values and interpolate for the missing values
nans, indexes= nan_helper(signal)
signal[nans]= np.interp(indexes(nans), indexes(~nans), signal[~nans])

signallumen=signal[I]
signallumen_filtered=da.bandpassfilter(signal-np.mean(signal), fs, FB['cutoff1'], FB['cutoff2'])[I]


cuttedsignal=da.lowpassfilter(signal[I]-np.mean(signal[I]), fs, 15)+np.mean(signal[I])
error_highfreq=np.abs(cuttedsignal-signal[I])

cuttedsignal=da.highpassfilter(signal[I]-np.mean(signal[I]), fs, 4)+np.mean(signal[I])



## Peak to peak analysis



ampl,period,tvalley,tpeak,mean=da.amp_analysis(signallumen,signallumen_filtered,time,FB['cutoff1'], FB['cutoff2'],export=[],outputdir=[])



print('amplitude max (pc) =',max(np.array(ampl)/np.array(mean)*100))


### error between raw signal and lowpass filtered signal
error=[]
errormax=[]
for i in range(0,len(ampl)) :
    filter=(time<=tvalley[i]+period[i])&(time>=tvalley[i])
    error.append(np.sum(error_highfreq[filter])/sum(filter))
    errormax.append(np.max(error_highfreq[filter]))


### tortuosity and variability of the highpass filtered signal
tortuosity=[]
rawvar=[]
for i in range(0,len(ampl)) :
    filter=(time<=tvalley[i]+period[i])&(time>=tvalley[i])
    tortuosity.append(arc_length(time[filter], cuttedsignal[filter]))
    rawvar.append(cuttedsignal[filter].std())    
    
    
    
if FB['bandname']=='cardiac':
    varamp=[]
    varperiod=[]
    for i in range(0,len(ampl)) :
        #measure the variability of the cardiac pulsation over 1 second
        filter=(tvalley<=tvalley[i]+0.5)&(tvalley>=tvalley[i]-0.5)
        varamp.append(ampl[filter].std())
        varperiod.append(period[filter].std())


plt.figure()
plt.plot(time,signallumen, ':r',label='lumen raw signal')    
plt.plot(time,signallumen_filtered+np.mean(signallumen),'r',label='lumen filtered')
#plt.plot(time,signalendfoot-0.5, ':g',label='raw signal')    
#plt.plot(time,signalendfoot_filtered+np.mean(signalendfoot)-0.5,'g',label='cardiac band filtered')
plt.xlabel('time (s)')
plt.ylabel('radius (um)')

plt.figure()
plt.hist(error)
plt.hist(error1)
plt.title('error')

plt.figure()
plt.hist(errormax)
plt.hist(errormax1)
plt.title('error max')


plt.figure()
plt.hist(tortuosity)
plt.hist(tortuosity1)
plt.title('tortuosity')

plt.figure()
plt.hist(rawvar)
plt.hist(rawvar1)
plt.title('rawvar')

plt.figure()
# scanline='PVS'
# signal=data[scanline].values/2

# #must keep only non nan values and interpolate for the missing values
# nans, indexes= nan_helper(signal)
# signal[nans]= np.interp(indexes(nans), indexes(~nans), signal[~nans])

# signalpvs=signal[I]
# signalpvs_filtered=da.bandpassfilter(signal-np.mean(signal), fs, FB['cutoff1'], FB['cutoff2'])[I]


#plt.plot(time,signalpvs+3.5, ':b',label='PVS raw signal')   
#plt.plot(time,signalpvs_filtered+np.mean(signalpvs)+3.5, 'b',label='PVS filtered signal')   
#plt.plot(time,signalendfoot_filtered-signallumen_filtered+np.mean(signalendfoot)-np.mean(signallumen)+3,'b',label='from filtered lumen and endfoot')
#plt.plot(time,np.mean(signalendfoot)-signallumen_filtered-np.mean(signallumen)+2.5,'k--',label='pvs from lumen osc')

plt.xlabel('time (s)')
plt.ylabel('radius (um)')
#plt.legend()

plt.xlim([tmin,tmin+10/FB['cutoff1']])
#plt.xlim([456,456+10/FB['cutoff1']])
plt.show()







#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 14:12:40 2021

@author: alexandra
"""

import pickle
import pandas as pd
import os
from os import listdir
from os.path import isfile, join

import numpy as np

dir='../output/databases/PenetratingArteriolesWT9/detailled/'


files = [f for f in listdir(dir) if isfile(join(dir, f))]
    
studylist=[]
    
for f in files : 
        data=pd.read_pickle(dir+f)
        studylist.append(data)
        
        
       


amplitudedb=pd.concat([data for data in studylist], ignore_index=True)



#filter=(amplitudedb['linescan']=='PVS1')|(amplitudedb['linescan']=='PVS2')

#amplitudedb['linescan'][filter]='PVS'



#amplitudedb['robust']=False


filter=(amplitudedb['amp']>0)
filter=filter&(amplitudedb['mean']>0)
filter=filter&(amplitudedb['hasnan']==False)

# filter rigid motion 
#rigidmotion= (amplitudedb['bandname']=='VLF')*1.5 + (amplitudedb['bandname']=='LF')*0.8 + (amplitudedb['bandname']=='resp')*0.4 + (amplitudedb['bandname']=='cardiac')*0.4
#theta=np.arcsin(rigidmotion/amplitudedb['meanlumen'])
# rigidartefact=amplitudedb['meanlumen']*(1-np.cos(theta))
# filter=filter&(amplitudedb['amp']>rigidartefact)


# remove outliers
filter=filter& np.logical_not((amplitudedb['bandname']=='cardiac')&(amplitudedb['outlier']))


amplitudedb=amplitudedb[filter]

print(np.unique(amplitudedb[['linescan']]))
print(np.unique(amplitudedb[['stage']]))


amplitudedb.to_csv('/home/alexandra/Documents/Python/linescan-analysis/output/databases/PenetratingArtriolesWT9.csv', index=False)
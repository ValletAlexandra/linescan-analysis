#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 15:25:20 2021

@author: alexandra
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 16:45:48 2021

@author: alexandra
"""

import scipy.stats
import matplotlib.pyplot as plt
import scipy as sp
import numpy as np
import pandas as pd
import random

import os
from os import listdir
from os.path import isfile, join

from scipy import stats

#import pyDOE



def timeparams(f,h0,D) :

    tend=max(3/f,2*h0**2/D)
    dt=1/f/8
    toutput=1/f/4

    while tend/dt <100 :
            dt/=2      
            
    while tend/toutput <100 :
        toutput/=2
            
    Noutput=tend/toutput
    
    if Noutput>500 :
        print('WARNING, more than 500 outputs ! N=%i'%Noutput)
        
    return ' -tend %e --time_step %e -toutput %e'%(tend,dt,toutput)

mouse='WT'
study=''


D=2e-7

#f=open('/home/alexandra/Documents/Python/sleep/sleep/scripts/script_'+mouse+study+'.sh', 'w')

fileout=open('/home/alexandra/Documents/Python/sleep/sleep/scripts/script_WT.sh', 'w')



simucall='python3 ../fbb_DD/PVS_simulation.py'
diffusion=' --diffusion_coef %e'%D
default=' --resistance -1 -xi 100e-4  -lpvs 200e-4 -nr 8 --sigma 1e-4'
        

    
    
# linescan='lumen'
# datalumen=pd.read_pickle('../output/databases/amplitude_'+mouse+'-'+study+'-'+linescan+'.pkl')

# linescan='endfoot'
# dataendfoot=pd.read_pickle('../output/databases/amplitude_'+mouse+'-'+study+'-'+linescan+'.pkl')

# linescan='PVS'
# dataPVS=pd.read_pickle('../output/databases/amplitude_'+mouse+'-'+study+'-'+linescan+'.pkl')


dir='/home/alexandra/Documents/Python/linescan-analysis/output/databases/cleanWT/'

# get the list of files

files = [f for f in listdir(dir) if isfile(join(dir, f))]

studylist=[]

for f in files : 
    data=pd.read_pickle(dir+f)
    studylist.append(data)
    
#concanate


amplitudedb=pd.concat([data for data in studylist], ignore_index=True)


datalumen=amplitudedb[amplitudedb['linescan']=='lumen']
dataenfoot=amplitudedb[amplitudedb['linescan']=='endfoot']
dataPVS=amplitudedb[amplitudedb['linescan']=='PVS']


figno=1

if False :
    
    
    listpoints=[]
        
    
    
    for stage in ['REM','NREM','IS','Quiet','Locomotion','Whisking']:
        
        
        filtrePVS=(dataPVS['stage']==stage)&(dataPVS['bandname']=='VLF')
        IVLF=np.where(filtrePVS)[0] 
        
        noiteration=0
        
        for i in IVLF :
            noiteration+=1
            print('ajout du point %i/%i'%(noiteration,len(IVLF)))
            point={}
            # find the same oscillation in lumen
            filtre=(datalumen['stage']==stage)&(datalumen['bandname']=='VLF')&(datalumen['tmax']>dataPVS['tmin'].iloc[i])&(datalumen['tmax']<(dataPVS['tmin'].iloc[i]+dataPVS['period'].iloc[i]))
            ilumen=np.where(filtre)[0] 
            
            if len(ilumen) :
                point['stage']=stage
                point['t1']=dataPVS['tmin'].iloc[i]
                point['t2']=dataPVS['tmax'].iloc[i]
                point['Rv']=datalumen['mean'].iloc[ilumen].mean()
            
                
                point['h0']=dataPVS['mean'].iloc[i]
                point['VLF amp pc']=dataPVS['amp'].iloc[i]/dataPVS['mean'].iloc[i]
                point['VLF f']=1/dataPVS['period'].iloc[i]
            
                # now average the shorter oscillations during the VLF oscillation
                
                #LF
                
                filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']=='LF')&(dataPVS['tmin']>point['t1'])&(dataPVS['tmax']<(point['t1']+dataPVS['period'].iloc[i]))
                
                if sum(filtre) :            
                    #point['h0']=dataPVS['mean'][filtre].mean()                
                    point['LF amp pc']=(dataPVS['amp'][filtre]/dataPVS['mean'].iloc[i]).mean()
                    point['LF f']=(1/dataPVS['period'][filtre]).mean()
                    point['LF N']=sum(filtre)
                
                    # resp
                    filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']=='resp')&(dataPVS['tmin']>point['t1'])&(dataPVS['tmax']<(point['t1']+dataPVS['period'].iloc[i]))
                    
                    if sum(filtre) :
                        point['resp amp pc']=dataPVS['amp'][filtre].mean()/dataPVS['mean'].iloc[i]
                        point['resp f']=  1/dataPVS['period'][filtre].mean() 
                        point['LF N']=sum(filtre)
                
                        #Cardiac
                        filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']=='cardiac')&(dataPVS['tmin']>point['t1'])&(dataPVS['tmax']<(point['t1']+dataPVS['period'].iloc[i]))
                        
                        if sum(filtre) :
                            point['cardiac f']=(1/dataPVS['period'][filtre]).mean()
                            point['cardiac amp pc']=(dataPVS['amp'][filtre]/dataPVS['mean'].iloc[i]).mean()
                            point['cardiac N']=sum(filtre)
                        
                            listpoints.append(point)
                    
listpoints= pd.DataFrame(listpoints)





for stage in ['REM','NREM','IS','Quiet','Locomotion','Whisking']:
    print('Generate simulation script '+stage)
    
    # We keep only points within +- two std
    filter=(listpoints['stage']==stage)
    
    
    subfilter=(np.abs(stats.zscore(np.log(listpoints[['Rv', 'h0', 'VLF amp pc', 'VLF f', 'LF amp pc',
           'LF f', 'resp amp pc', 'resp f', 'cardiac f', 'cardiac amp pc']][filter]))) < 1).any(axis=1)
    
    filter=np.where(filter)[0][subfilter]

    for bandname, name in zip(['cardiac','resp','LF','VLF'],['Cardiac','Respiratory','LF','VLF']):
        #mean value
        job=' -j '+mouse+'-'+study+'-'+stage+'-'+bandname+'_mean' 
        amplitudes=' -ai %e'%(listpoints[filter][bandname+' amp pc'].mean())
        frequences=' -fi %e'%listpoints[filter][bandname+' f'].mean()
        lumenradius=' -rv %e'%(listpoints[filter]['Rv'].mean()*1e-4)
        pvsradius= ' -rpvs %e'%((listpoints[filter]['Rv']+listpoints[filter]['h0']).mean()*1e-4)
        
        fileout.write(simucall+job+default+amplitudes+frequences+lumenradius+pvsradius+diffusion+timeparams(listpoints[bandname+' f'].mean(),listpoints['h0'].mean()*1e-4,D)+'&\n')    
                   
        
        N=len(listpoints[filter])  
        try:
            sample=random.sample(range(N), 20) ### Can change the number of points here
        except ValueError:
            sample=range(N)
            
        sample=np.where(filter)[0][sample]

        pointnumber=0
        for i in sample :
    
            job=' -j '+mouse+'-'+study+'-'+stage+'-'+bandname+'_pt'+str(pointnumber) 
            amplitudes=' -ai %e'%listpoints[bandname+' amp pc'].iloc[i]
            frequences=' -fi %e'%listpoints[bandname+' f'].iloc[i]
            lumenradius=' -rv %e'%(listpoints['Rv'].iloc[i]*1e-4)
            pvsradius= ' -rpvs %e'%((listpoints['Rv'].iloc[i]*1e-4)+(listpoints['h0'].iloc[i]*1e-4))
 
            fileout.write(simucall+job+default+amplitudes+frequences+lumenradius+pvsradius+diffusion+timeparams(listpoints[bandname+' f'].iloc[i],listpoints['h0'].iloc[i]*1e-4,D)+'&\n')
            
            pointnumber+=1
            
        fileout.write('wait \n')
        
        plt.figure(figno)
        plt.hist(listpoints[bandname+' amp pc'].iloc[sample],bins=20,alpha=.9, density=True,label='simulated distribution')
        plt.xlabel(stage+' '+name+' oscillation amplitude (um)')
        plt.ylabel('Probability density')
        plt.legend()
        figno+=1
        
        plt.figure(figno)
        plt.hist(listpoints[bandname+' f'].iloc[sample],bins=20,alpha=.9, density=True,label='simulated distribution')
        plt.xlabel(stage+' '+name+' oscillation frequency (Hz)')
        plt.ylabel('Probability density')
        plt.legend()
        figno+=1
        
        plt.figure(figno)
        plt.hist(listpoints['Rv'].iloc[sample],bins=20,alpha=.9, density=True,label='simulated distribution')
        plt.xlabel(stage+' '+name+' Rv')
        plt.ylabel('Probability density')
        plt.legend()
        figno+=1

        plt.figure(figno)
        plt.hist(listpoints['h0'].iloc[sample],bins=20,alpha=.9, density=True,label='simulated distribution')
        plt.xlabel(stage+' '+name+' h0')
        plt.ylabel('Probability density')
        plt.legend()
        figno+=1        
            
        
        
fileout.close()
    
#     samp=datalumen[filtre]['mean']
#     param=sp.stats.lognorm.fit(samp, floc=0) # fit the sample data
    
#     x=np.linspace(max(0,np.mean(samp)-3*np.std(samp)),np.mean(samp)+3*np.std(samp),100)
#     pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=0, scale=param[2]) # fitted distribution
#     Rv=scipy.stats.lognorm.ppf(LHS[:,0], param[0], loc=0, scale=param[2])
#     line1+=(' %.1e (%.1e) |'% (param[2],param[0]))  
    
#     Rvmean=param[2]
    
#     plt.figure(figno)
#     plt.plot(x,pdf_fitted,'k-',label='fitted distribution')
#     plt.plot(Rv,Rv*0,'xk',label='simulation points')
#     plt.hist(samp,bins=20,alpha=.9, density=True,label='measurment distribution')
#     plt.hist(Rv,bins=5,alpha=.5, density=True,label='simulation distribution')
#     plt.xlabel('Mean lumen radius (um)')
#     plt.ylabel('Probability density')
#     plt.legend()
#     figno+=1
    
#     filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']=='LF')
#     samp=dataPVS[filtre]['mean']
#     param=sp.stats.lognorm.fit(samp, floc=0) # fit the sample data
    
    
    
#     x=np.linspace(max(0,np.mean(samp)-3*np.std(samp)),np.mean(samp)+3*np.std(samp),100)
#     pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=0, scale=param[2]) # fitted distribution
#     h0=scipy.stats.lognorm.ppf(LHS[:,1], param[0], loc=0, scale=param[2])
#     line2+=(' %.1e (%.1e) |'% (param[2],param[0]))
    
#     h0mean=param[2]
   
#     plt.figure(figno)
#     plt.plot(x,pdf_fitted,'k-',label='fitted distribution')
#     plt.plot(h0,h0*0,'xk',label='simulation points')
#     plt.hist(samp,bins=20,alpha=.9, density=True,label='measurment distribution')
#     plt.hist(h0,bins=5,alpha=.5, density=True,label='simulation distribution')
#     plt.xlabel('Mean PVS thickness (um)')
#     plt.ylabel('Probability density')
#     plt.legend()
#     figno+=1
    
    
    
#     pdf_amp={}
#     pdf_freq={}
    

#     for bandname, name in zip(['cardiac','resp','LF','VLF'],['Cardiac','Respiratory','LF','VLF']):
#         filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']==bandname)
#         samp=dataPVS[filtre]['amp']
#         param=sp.stats.lognorm.fit(samp,floc=0 ) # fit the sample data

#         x=np.linspace(max(0,np.mean(samp)-3*np.std(samp)),np.mean(samp)+3*np.std(samp),100)
#         pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=0, scale=param[2]) # fitted distribution
#         amp=scipy.stats.lognorm.ppf(LHS[:,2], param[0], loc=0, scale=param[2])
        
#         ampmean=param[2]
        
#         plt.figure(figno)
#         plt.plot(x,pdf_fitted,'k-',label='fitted distribution')
#         plt.plot(amp,amp*0,'xk',label='simulation points')
#         plt.hist(samp,bins=20,alpha=.9, density=True,label='measurment distribution')
#         plt.hist(amp,bins=5,alpha=.5, density=True,label='simulation distribution')


#         line3[bandname]+=(' %.1e (%.1e) |'% (param[2],param[0]))
#         plt.xlabel(name+' oscillation amplitude (um)')
#         plt.ylabel('Probability density')
#         plt.legend()
#         figno+=1
        

    
#         filtre=(dataPVS['stage']==stage)&(dataPVS['bandname']==bandname)
#         samp=1/dataPVS[filtre]['period']
#         param=sp.stats.lognorm.fit(samp, floc=0) # fit the sample data
        
#         x=np.linspace(max(0,np.mean(samp)-3*np.std(samp)),np.mean(samp)+3*np.std(samp),100)
#         pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=0, scale=param[2]) # fitted distribution
#         freq=scipy.stats.lognorm.ppf(LHS[:,3], param[0], loc=0, scale=param[2])
        
#         freqmean=param[2]
        
#         plt.figure(figno)
#         plt.plot(x,pdf_fitted,'k-',label='fitted distribution')
#         plt.plot(freq,freq*0,'xk',label='simulation points')
#         plt.hist(samp,bins=20,alpha=.9, density=True,label='measurment distribution')
#         plt.hist(freq,bins=5,alpha=.5, density=True,label='simulation distribution')
        
        
#         line4[bandname]+=(' %.1e (%.1e) |'% (param[2],param[0]))
#         plt.xlabel(name+' oscillation frequency)')
#         plt.ylabel('Probability density')
#         plt.legend()
#         figno+=1
        
#         print('Generate simulation script '+stage)
#         job=' -j '+mouse+'-'+study+'-'+stage+'-'+bandname+'_mean' 
#         amplitudes=' -ai %e'%(ampmean/Rvmean)
#         frequences=' -fi %e'%freqmean
#         lumenradius=' -rv %e'%(Rvmean*1e-4)
#         pvsradius= ' -rpvs %e'%(Rvmean*1e-4+h0mean*1e-4)
        
#         f.write(simucall+job+default+amplitudes+frequences+lumenradius+pvsradius+diffusion+timeparams(freqmean,h0mean*1e-4,D)+'&\n')

#         for i in range(N) :
     
#             job=' -j '+mouse+'-'+study+'-'+stage+'-'+bandname+'_pt'+str(i) 
#             amplitudes=' -ai %e'%(amp[i]/h0[i])
#             frequences=' -fi %e'%freq[i]
#             lumenradius=' -rv %e'%(Rv[i]*1e-4)
#             pvsradius= ' -rpvs %e'%(Rv[i]*1e-4+h0[i]*1e-4)
 
#             f.write(simucall+job+default+amplitudes+frequences+lumenradius+pvsradius+diffusion+timeparams(freq[i],h0[i]*1e-4,D)+'&\n')
        
#         f.write('wait \n')
            
        
# f.close()


        
# markdown.append('## Mean lumen radius and PVS thickness')

# markdown.append('| Variable |         Description | Distribution | REM median (std)  | NREM median (std) | IS median (std) |')

# markdown.append('|----------|---------------------|--------------|----------------|-----------------| --------------|')



# markdown.append(line1)
# markdown.append(line2)

# for bandname, name in zip(['cardiac','resp','LF','VLF'],['Cardiac','Respiratory','LF','VLF']):


#     markdown.append('\n')
#     markdown.append('## '+name+' oscillations')
#     markdown.append('\n')
    
#     markdown.append('| Variable |         Description | Distribution | REM median (std) | NREM median (std) | IS median (std) |')
    
#     markdown.append('|----------|---------------------|--------------|----------------|-----------------| --------------|')
    
#     markdown.append(line3[bandname])
#     markdown.append(line4[bandname])

#     markdown.append('\n')
#     markdown.append('\n')

# for l in markdown :
#     print(l)
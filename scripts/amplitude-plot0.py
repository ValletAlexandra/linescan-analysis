#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 15 00:06:02 2021

@author: alexandra
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 14:12:50 2021

@author: alexandra
"""

import pickle
import pandas as pd
from matplotlib import pyplot as plt 
from scipy.stats import mannwhitneyu, normaltest
import seaborn as sbn
from statannot import add_stat_annotation
import numpy as np

from os import listdir
from os.path import isfile, join

from scipy import stats

import itertools

sbn.set(style="whitegrid")  

#amplitudedb[amplitudedb['amp pc']>50][['mouse number','trial','bandname','tmin','amp pc']]


All=False
box=False
hist=False
violin=True

colors=['darkorchid','mediumseagreen','darkorange','blue']
my_pal = {"REM": "darkorchid", 
          "NREM": "seagreen", 
          "IS":"darkorange", 
          "Baseline":"royalblue",
          "REM WT": "darkorchid", 
          "NREM WT": "seagreen", 
          "IS WT":"darkorange", 
          "Baseline WT":"royalblue",
          "REM AQP4KO": "mediumorchid", 
          "NREM AQP4KO": "mediumseagreen", 
          "IS AQP4KO":"moccasin", 
          "Baseline AQP4KO":"cornflowerblue"}

legends={'period':'Oscillation period (s)','amp':'Oscillation amplitude (um)','mean':'Radius mean value','amp pc':'Oscillation amplitude (pc)','amp norm':'Normalized amplitude','period norm':'Normalized period','mean norm':'Normalized mean radius' }
        
bandnamelist=['cardiac','resp','LF','VLF']
variablelist=['amp pc','period','mean norm']

comparison_variable='stage'

stage_list={'stage':['Baseline', 'NREM','IS','REM'], 'stage mouse':['Baseline WT','Baseline AQP4KO', 'NREM WT','NREM AQP4KO','IS WT','IS AQP4KO','REM WT','REM AQP4KO']}

combinations={'stage':[('Baseline', 'NREM'),
                     ('Baseline', 'IS'),
                     ('Baseline', 'REM')], 
 'stage mouse':[('Baseline WT','Baseline AQP4KO'),
                ('NREM WT','NREM AQP4KO'),
                ('IS WT','IS AQP4KO'),
                ('REM WT','REM AQP4KO'),
                ('Baseline WT', 'NREM WT'),
                ('Baseline WT', 'IS WT'),
                ('Baseline WT', 'REM WT'),
                ('Baseline AQP4KO', 'NREM AQP4KO'),
                ('Baseline AQP4KO', 'IS AQP4KO'),
                ('Baseline AQP4KO', 'REM AQP4KO')]}


for scanline in ['lumen']: #,'lumen','endfoot'   

    ###get data
    
    # # just one scanline
    
    # mousekind='AQP4KO'
    # mousenumber='04'
    # trial='16'
    # nofinal=''
    # study=mousekind+'-'+mousenumber+'-'+trial+'-'+nofinal

    # amplitudedb=pd.read_pickle('../output/databases/amplitude_'+mousekind+'-'+mousenumber+'-'+trial+'-'+nofinal+'-'+scanline+'.pkl')
        
    
    # #several linescans

    # set the data directory 
    #dir='/home/alexandra/Documents/Python/scanline-analysis/output/databases/'
    dir='/home/alexandra/Documents/Python/linescan-analysis/output/databases/oldanalysislist/'
    #dir='/home/alexandra/Documents/Python/linescan-analysis/output/databases/oldsleepWToldmean/'
    # get the list of files
    
    files = [f for f in listdir(dir) if isfile(join(dir, f))]
    
    studylist=[]
    
    for f in files : 
        data=pd.read_pickle(dir+f)
        studylist.append(data)
        
    #concanate
    study='all'

    amplitudedb=pd.concat([data for data in studylist], ignore_index=True)
    
    

    # add new variables
    amplitudedb['amp pc']=amplitudedb['amp']/amplitudedb['mean']*100
    
    amplitudedb['stage mouse']=amplitudedb['stage'] +' '+amplitudedb['mouse kind']
    

    # filter
    # remove outliers
    filter=(np.abs(stats.zscore(amplitudedb[['amp pc','period','mean']])) < 2).any(axis=1)
    
    filter=filter &(amplitudedb['linescan']==scanline)
   
    #filtermice=((amplitudedb['mouse kind']=='WT')&(amplitudedb['mouse number']=='10'))|((amplitudedb['mouse kind']=='AQP4KO')&(amplitudedb['mouse number']=='04'))
    #filter=filter &filtermice
    
    #filter=filter &(np.abs(amplitudedb['amp pc'])<=100)
    
    #filter=filter&(amplitudedb['mouse kind']=='AQP4KO')
    
    
    if sum(filter) ==0:
        break
    
    amplitudedb=amplitudedb[filter]


            
    for bandname in bandnamelist:
    
        for variable in variablelist:
            
            if hist :
        
                plt.figure() 
                for stage,c in zip(stage_list[comparison_variable],colors):
                    filter=(amplitudedb[comparison_variable]==stage)&(amplitudedb['bandname']==bandname)
                    
                    dataplot=amplitudedb[filter]
                    
                    
                    width=(max(amplitudedb[variable][amplitudedb['bandname']==bandname])-min(amplitudedb[variable][amplitudedb['bandname']==bandname]))/15
                    sbn.histplot(data=dataplot,x=variable,palette="crest",color=c,label=stage,stat="percent",binwidth=width)
                
                
                
                plt.xlabel(legends[variable])
                plt.legend()
                plt.title(bandname+' '+study+' '+scanline)
                
                plt.savefig('../output/images/'+linescan+'/'+study+'-'+bandname+'_'+variable+'_hist.png')
                #plt.savefig('../output/images/'+linescan+'/'+study+'-'+bandname+'_'+variable+'_hist.svg')
                
                
            
            
            filter=(amplitudedb['bandname']==bandname)
            dataplot=amplitudedb[filter]
            dataplot.replace([np.inf, -np.inf], np.nan, inplace=True)
            dataplot.dropna(axis=0,how='any')
            
            
            if box :
            
                plt.figure()
                
                yposlist = dataplot.groupby([comparison_variable])[variable].median().tolist()
                xposlist = np.array(range(len(yposlist)))+0.02
                stringlist = ['%.1e'%m for m in yposlist]
                
                
                ax = sbn.boxplot(x=comparison_variable, y=variable, data=dataplot,palette=my_pal)  
                plt.ylabel(legends[variable])
                plt.title(bandname+' '+study+' '+scanline)
                
                for i in range(len(stringlist)):
                    ax.text(xposlist[i], yposlist[i], stringlist[i])
    
                #combinations=list(itertools.combinations(list(np.unique(dataplot[comparison_variable])), 2))
                
                
                add_stat_annotation(ax, data=dataplot, x=comparison_variable, y=variable, order=list(np.unique(dataplot[comparison_variable])),box_pairs=combinations[comparison_variable],
                                    test='Mann-Whitney', text_format='star',verbose=2)
                
                plt.tight_layout()
                
                plt.savefig('../output/images/'+scanline+'/'+study+'-'+bandname+'_'+variable+'_box.png')
                #plt.savefig('../output/images/'+linescan+'/'+study+'-'+bandname+'_'+variable+'_box.svg')
                
            if violin :
            
                plt.figure(figsize=(10, 5)) 
                
                
                yposlist = dataplot.groupby([comparison_variable])[variable].median().tolist()
                xposlist = np.array(range(len(yposlist)))+0.02
                stringlist = ['%.1e'%m for m in yposlist]
                
                
                ax=sbn.violinplot(x=comparison_variable, y=variable, data=dataplot, order=list(np.unique(dataplot[comparison_variable])),alpha=0.3, palette=my_pal, cut=0)  
               # sbn.swarmplot(data=dataplot,x="stage", y=variable, color="white", edgecolor="gray")
                ax.set_xticklabels(ax.get_xticklabels(),rotation = 30)
                plt.ylabel(legends[variable])
                plt.title(bandname+' '+study+' '+scanline)
                
                for i in range(len(stringlist)):
                    ax.text(xposlist[i], yposlist[i], stringlist[i])
    
               # combinations=list(itertools.combinations(list(np.unique(dataplot[comparison_variable])), 2))
                
                add_stat_annotation(ax, data=dataplot, x=comparison_variable, y=variable, order=list(np.unique(dataplot[comparison_variable])),box_pairs=combinations[comparison_variable],
                                    test='Mann-Whitney', text_format='star',verbose=2)
                
                plt.tight_layout()
                
                plt.savefig('../output/images/'+scanline+'/'+study+'-'+bandname+'_'+variable+'_violin.png')
                #plt.savefig('../output/images/'+linescan+'/'+study+'-'+bandname+'_'+variable+'_violin.svg')

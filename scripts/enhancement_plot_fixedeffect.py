#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 15:28:50 2021

@author: alexandra
"""

import pandas as pd
import matplotlib.pyplot as plt
import scipy as sp
import numpy as np

import seaborn as sbn
from statannot import add_stat_annotation

import itertools

sbn.set(style="whitegrid")  

colors=['gray','darkorchid','mediumseagreen','darkorange']
#my_pal = {"REM": "darkorchid", "NREM": "mediumseagreen", "IS":"darkorange"}

my_pal = {"baseline":"gray","stageREM": "darkorchid", "stageNREM": "mediumseagreen", "stageIS":"darkorange"}


mouse='WT08'
study='03'
D=6e-7

#stages=['IS','NREM','REM']
stages=['baseline','stageNREM','stageIS','stageREM','stageAwakening']
stagesname=['baseline','NREM','IS','REM', 'Wake']


combis=list(itertools.combinations(list(np.unique(stages)), 2))

bandnames=['VLF','LF','resp','card-v1e-03']

#file_name='/home/alexandra/Documents/Python/linescan-analysis/output/disp_analysis/'+mouse+'-'+study+'.csv'




# read data 

file_name='/home/alexandra/Documents/Python/linescan-analysis/output/disp_analysis/disp-d2e-07-l6e-02FixedWT7t20area.csv'
data=pd.read_csv(file_name)


bandnames=np.unique(data['bandname'])
bandnames=[ 'VLF ', 'LF ','card-v5e-03 ']
stages=np.unique(data['stage'])

#file_name='/home/alexandra/Documents/Python/linescan-analysis/output/disp_analysis/disp-d6e-07-l2e-023.csv'
#data2=pd.read_csv(file_name)
# we remove card from this database
#data2=data2[data2['bandname']!='card']

#concanate
#data=pd.concat([data1,data2], ignore_index=True)


# remove absrud values for enhancement --> error of fit ?
data.loc[data['Rfit'] <0 , 'Rfit'] = 0
data.loc[data['RFWHM'] <0 , 'RFWHM'] = 0


#fig2,axs2 = plt.subplots(len(stages), len(bandnames))

fig2=plt.figure()
gs=fig2.add_gridspec(len(stages), len(bandnames)+1, hspace=0, wspace=0)
axs2=gs.subplots(sharex='col', sharey='row')

theta = np.linspace(0, 2*np.pi, 100)

emax=0.5


data['u max (um/s)']= data['umax']*1e4
data['p max (Pa)']= data['pmax']/10

data[variable]


variable='RFWHM'
#variable='Rfit'
#variable='Pe'

# test with histograms
for i,stage in enumerate(stages) :
    for j, bandname in enumerate(bandnames) :
        filter=(data['bandname']==bandname)&(data['stage']==stage)
        Enhancement=data[variable][filter]
        print(stage)
        print(bandname)
        print(Enhancement)
        
        #axs1[i, j].hist(Enhancement)
        
        #param=sp.stats.lognorm.fit(Enhancement,loc=0 ) # fit the sample data

        #x=np.linspace(max(0,np.mean(Enhancement)-1*np.std(Enhancement)),np.mean(Enhancement)+1*np.std(Enhancement),100)
        #x=np.linspace(0,emax*2,100)
        #pdf_fitted = sp.stats.lognorm.pdf(x, param[0], loc=param[1], scale=param[2])# fitted distribution
        
        #pdf_fitted=pdf_fitted-pdf_fitted.min()
        #pdf_fitted=pdf_fitted/pdf_fitted.max() 
        
        #for e,level in zip(x,pdf_fitted) :
        #    axs2[i, j].plot( e*np.cos(theta), e*np.sin(theta), 'b',alpha=level,linewidth=1)
        
        for e in Enhancement :
            disk=plt.Circle((0,0),e, color='b')
            axs2[i, j].add_patch(disk)
        
        #emean=np.median(Enhancement)
        #emean=emean.mean()
        #axs2[i, j].plot( emean*np.cos(theta), emean*np.sin(theta), 'k',linewidth=1)
        
        axs2[i, j].set_aspect(1)
        axs2[i, j].set_xlim([-emax,emax])
        axs2[i, j].set_ylim([-emax,emax])
        
        print(stage)
        print(bandname)
        axs2[i, j].set_xlabel(bandname)
        axs2[i, j].set_ylabel(stage[5::])
        
        # make xaxis invisibel
        #axs2[i, j].xaxis.set_visible(False)
        # make spines (the box) invisible
        #plt.setp(axs2[i, j].spines.values(), visible=False)
        # remove ticks and labels for the left axis
        axs2[i, j].tick_params(left=False, labelleft=False)# remove ticks and labels for the left axis
        axs2[i, j].tick_params(bottom=False, labelbottom=False)
        
    # add the total

    filter=(data['stage']==stage)
    filterfb=(data['bandname']==bandnames[0])
    for bandname in bandnames[1::]:
        filterfb=filterfb|(data['bandname']==bandname)
    filter=filter&filterfb
    Enhancement=[data[variable][filter].sum()]

    
    for e in Enhancement :
         disk=plt.Circle((0,0),e, color='b')
         axs2[i, len(bandnames)].add_patch(disk)

        
    
    axs2[i, len(bandnames)].set_aspect(1)
    axs2[i, len(bandnames)].set_xlim([-emax,emax])
    axs2[i, len(bandnames)].set_ylim([-emax,emax])

    axs2[i, len(bandnames)].set_xlabel('Total')
    axs2[i, len(bandnames)].set_ylabel(stage[5:])
        
    # make xaxis invisibel
    #axs2[i, len(bandnames)].xaxis.set_visible(False)
    # make spines (the box) invisible
    plt.setp(axs2[i, len(bandnames)].spines.values(), visible=False)
    # remove ticks and labels for the left axis
    axs2[i, len(bandnames)].tick_params(left=False, labelleft=False)# remove ticks and labels for the left axis
    axs2[i, len(bandnames)].tick_params(bottom=False, labelbottom=False)
    
        

for ax in axs2.flat:
    ax.label_outer()
    
    
for j, bandname in enumerate(bandnames) :
        filter=(data['bandname']==bandname)


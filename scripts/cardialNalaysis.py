#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 13:02:12 2022

@author: alexandra
"""
import numpy as np
from matplotlib import pyplot as plt 
plt.ioff()

import pandas as pd
import scipy.signal as sg
import os

import src.datanalysis as da

from matplotlib.patches import Rectangle

from os import listdir
from os.path import isfile, join

import regex as re


#select study name

# location of the database peak to peak analysis 
dirdatabase='/home/alexandra/Documents/Python/linescan-analysis/output/databases/pialsWT/'

#location of the raw data
dirraw='/home/alexandra/Documents/Data/Laura/Linescans/10012022/PenetratingArterioles/WT/'

### get the database with the peak to peak analysis

# set the data directory 


files = [f for f in listdir(dirdatabase) if isfile(join(dirdatabase, f))]
    
studylist=[]
    
for f in files : 
    data=pd.read_pickle(dirdatabase+f)
    studylist.append(data)
        
    #concanate

    amplitudedb=pd.concat([data for data in studylist], ignore_index=True)
    
    
